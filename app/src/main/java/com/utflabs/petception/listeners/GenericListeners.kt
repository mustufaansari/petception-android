package com.utflabs.petception.listeners

import android.widget.EditText

/**
 * created by Mustufa Ansari on 19,August,2020
 * Email : mustufaayub82@gmail.com
 */
interface GenericListeners {


    fun onTapLogin() {}

    fun onTapSignUp() {}

    fun onTapDateOfBirth(view: EditText) {}

    fun onTapGoogleConnect() {}

    fun onTapFacebookConnect() {}

    fun onTapDisConnectGoogleAccount() {}

    fun onTapDisConnectFacebookAccount() {}

    fun onTapOpenSettingsForWifi() {}

    fun onTapOpenSettingsForPermission() {}

    fun onTapGoLive() {}

    fun onTapStopLive() {}

    fun onTapClose() {}

    fun onTapRefresh(){}

    fun onTapFacebookLive() {}

    fun onTapYoutubeLive() {}
}