package com.utflabs.petception.listeners

interface DialogListeners {
    /**
     * When User Tap On Positive Button
     */
    fun onPositiveButtonTap() {}

    /**
     * When User Tap On Negative Button
     */
    fun onNegativeButtonTap() {}


}