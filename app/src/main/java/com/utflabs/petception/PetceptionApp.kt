package com.utflabs.petception

import android.content.Context
import androidx.lifecycle.LifecycleObserver
import com.facebook.FacebookSdk
import com.utflabs.petception.broadcast.InternetConnectivityListener
import com.petception.base.DaggerApp
import org.eclipse.paho.client.mqttv3.*


/**
 * created by Mustufa Ansari on 18,August,2020
 * Email : mustufaayub82@gmail.com
 */
class PetceptionApp : DaggerApp(), LifecycleObserver,MqttCallback {
    override fun onCreate() {
        super.onCreate()

        if (application == null) {
            application = this
        }
        FacebookSdk.sdkInitialize(applicationContext)
    }

    fun setConnectivityListener(listener: InternetConnectivityListener.ConnectivityReceiverListener?) {
        InternetConnectivityListener.connectivityReceiverListener = listener
    }

    companion object {
        var application: PetceptionApp? = null
            private set

        val context: Context
            get() = application!!.applicationContext
    }

    override fun messageArrived(topic: String?, message: MqttMessage?) {

    }

    override fun connectionLost(cause: Throwable?) {
    }

    override fun deliveryComplete(token: IMqttDeliveryToken?) {
    }
}