package com.utflabs.petception.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.wifi.WifiManager
import android.os.Build
import com.petception.base.model.response.Wifi
import com.utflabs.petception.R
import com.petception.base.utils.InjectUtils
import com.utflabs.petception.utils.Constants
import org.apache.commons.lang3.StringUtils


/**
 * Created by  Mustufa Ansari on 13/10/2020.
 * Copyright (c) 2020 All rights reserved.
 * email  : mustufaayub82@gmail.com
 */
/**
 * this class will be use to listen network changes in whole android app
 */
class InternetConnectivityListener : BroadcastReceiver() {
    var isConnectToWIFI: Boolean = false
    var isConnectToMobile: Boolean = false
    var bssid: String = StringUtils.EMPTY
    var ssID: String = StringUtils.EMPTY
    var wifi: Wifi? = null

    companion object {
        var connectivityReceiverListener: ConnectivityReceiverListener? = null

    }

    override fun onReceive(context: Context?, intent: Intent?) {
        val cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val wifiManager =
            context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        if (!intent?.action.isNullOrEmpty() && intent?.action == Constants.Actions.ON_CONECTIVITY_CHANGED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val networkCapabilities = cm.activeNetwork
                val actNw = cm.getNetworkCapabilities(networkCapabilities)
                actNw?.let {
                    isConnectToWIFI = it.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                    isConnectToMobile = it.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                    ssID = wifiManager.connectionInfo.ssid
                    bssid =
                        if (wifiManager.connectionInfo.bssid != null) wifiManager.connectionInfo.bssid else ""
                } ?: kotlin.run {
                    isConnectToWIFI = false
                    isConnectToMobile = false
                    ssID = context.getString(R.string.no_wifi_connected)
                }
                wifi = Wifi(ssID, bssid, isConnectToWIFI, isConnectToMobile)
            } else {
                cm.activeNetworkInfo.let { networkInfo ->
                    networkInfo?.run {
                        isConnectToWIFI = when (type) {
                            ConnectivityManager.TYPE_WIFI -> true
                            else -> false
                        }
                        isConnectToMobile = when (type) {
                            ConnectivityManager.TYPE_MOBILE -> true
                            else -> false
                        }
                        ssID = wifiManager.connectionInfo.ssid
                        bssid =
                            if (wifiManager.connectionInfo.bssid != null) wifiManager.connectionInfo.bssid else ""
                    }
                } ?: kotlin.run {
                    isConnectToWIFI = false
                    isConnectToMobile = false
                    ssID = context.getString(R.string.no_wifi_connected)

                }
                wifi = Wifi(ssID, bssid, isConnectToWIFI, isConnectToMobile)
            }
            if (connectivityReceiverListener != null) {
                connectivityReceiverListener!!.onNetworkConnectionChanged(wifi!!)
            }
        }
    }

    interface ConnectivityReceiverListener {
        fun onNetworkConnectionChanged(wifi: Wifi)
    }
}