package com.utflabs.petception.extensions

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.utflabs.petception.factory.ViewModelFactory
import com.utflabs.petception.service.PushStreamLiveService
import com.utflabs.petception.utils.Constants.Actions.STARTFOREGROUND_ACTION
import com.utflabs.petception.utils.Constants.Actions.STOPFOREGROUND_ACTION

/**
 * created by Mustufa Ansari on 18,August,2020
 * Email : mustufaayub82@gmail.com
 */

fun <T : ViewModel> AppCompatActivity.obtainViewModel(viewModelClass: Class<T>) =
    ViewModelProviders.of(this, ViewModelFactory.getInstance()).get(viewModelClass)

fun <T : AppCompatActivity> AppCompatActivity.gotoActivity(targetActivityClass: Class<T>) {
    val intent = Intent(this, targetActivityClass)
    startActivity(intent)
}

fun AppCompatActivity.gotoActivityWithNoHistory(targetActivityClass: Class<*>) {
    val i = Intent(this, targetActivityClass)
    i.flags =
        Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
    startActivity(i)
}

fun <T : AppCompatActivity> AppCompatActivity.gotoActivityWithNoHistory(
    targetActivityClass: Class<T>,
    intentKey: String,
    intentValue: Any? = null,
    intentKey2: String,
    intentValue2: Any? = null
) {
    val i = Intent(this, targetActivityClass)
    i.putExtra(intentKey, intentValue)
    i.putExtra(intentKey2, intentValue2)
    i.flags =
        Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
    startActivity(i)
}

fun AppCompatActivity.gotoActivity(
    targetActivityClass: Class<*>,
    intentKey: String,
    intentValue: Any? = null
) {
    val i = Intent(this, targetActivityClass)
    i.putExtra(intentKey, intentValue)
    startActivity(i)
}

fun AppCompatActivity.gotoActivity(
    targetActivityClass: Class<*>,
    intentKey1: String,
    intentValue1: Any? = null,
    intentKey2: String,
    intentValue2: Any? = null
) {
    val i = Intent(this, targetActivityClass)
    i.putExtra(intentKey1, intentValue1)
    i.putExtra(intentKey2, intentValue2)
    startActivity(i)
}

fun AppCompatActivity.gotoActivity(
    targetActivityClass: Class<*>,
    intentKey1: String,
    intentValue1: Any? = null,
    intentKey2: String,
    intentValue2: Any? = null,
    intentKey3: String,
    intentValue3: Any? = null
) {
    val i = Intent(this, targetActivityClass)
    i.putExtra(intentKey1, intentValue1)
    i.putExtra(intentKey2, intentValue2)
    i.putExtra(intentKey3, intentValue3)
    startActivity(i)
}

fun AppCompatActivity.openSettingsIntent() {
    val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
    val uri: Uri = Uri.fromParts("package", packageName, null)
    intent.data = uri
    startActivityForResult(intent, 101)
}

fun AppCompatActivity.openWiFiIntent() {
    val intent = Intent(Settings.ACTION_WIFI_SETTINGS)
    startActivity(intent)
}

fun AppCompatActivity.startPushingLiveServiceOnYouTube() {
    val intent = Intent(this, PushStreamLiveService::class.java)
    intent.action = STARTFOREGROUND_ACTION
    startServices(intent)
}

fun AppCompatActivity.stopPushingLiveServiceOnYouTube() {
    val intent = Intent(this, PushStreamLiveService::class.java)
    intent.action = STOPFOREGROUND_ACTION
    stopServices(intent)
}


private fun AppCompatActivity.stopServices(intent: Intent) {
    stopService(intent)
}

private fun AppCompatActivity.startServices(intent: Intent) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        startForegroundService(intent)
    } else {
        startService(intent)

    }
}

