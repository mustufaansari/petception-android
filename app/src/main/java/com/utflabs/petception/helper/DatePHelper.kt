package com.utflabs.petception.helper

import android.app.Activity
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Context
import android.widget.DatePicker
import android.widget.EditText
import android.widget.ImageView
import java.text.SimpleDateFormat
import java.util.*

class DatePHelper : OnDateSetListener {
    private var _editText: EditText
    private var _day = 0
    private var _month = 0
    private var _birthYear = 0
    private var _context: Context
    private var imageBtn: ImageView? = null
    private var dialog: DatePickerDialog? = null
    private var setMinMax: String? = null

    //    private Activity context;
    private var context: Context
    private var calendar: Calendar

    constructor(
        context: Activity,
        _editText: EditText,
        imageBtn: ImageView?,
        setMinMax: String?
    ) {
        this.context = context
        this._editText = _editText
        _context = context
        this.imageBtn = imageBtn
        this.setMinMax = setMinMax
        calendar = Calendar.getInstance(TimeZone.getDefault())
    }

    constructor(context: Activity, _editText: EditText) {
        this.context = context
        this._editText = _editText
        _context = context
        calendar = Calendar.getInstance(TimeZone.getDefault())
    }

    constructor(context: Context, _editText: EditText) {
        this.context = context
        this._editText = _editText
        _context = context
        calendar = Calendar.getInstance(TimeZone.getDefault())
    }

    constructor(
        context: Activity,
        _editText: EditText,
        imageBtn: ImageView?,
        date: Date?,
        setMinMax: String?
    ) {
        this.context = context
        this._editText = _editText
        _context = context
        this.imageBtn = imageBtn
        this.setMinMax = setMinMax
        calendar = Calendar.getInstance(TimeZone.getDefault())
        calendar.time = date
    }

    fun showDialog() {
        dialog = DatePickerDialog(
            _context, this,
            calendar[Calendar.YEAR], calendar[Calendar.MONTH],
            calendar[Calendar.DAY_OF_MONTH]
        )
        /*if (setMinMax.equals("setMin", ignoreCase = true)) {
            dialog!!.datePicker.maxDate = calendar.timeInMillis - 10000
        } else {
            dialog!!.datePicker.minDate = calendar.timeInMillis
        }*/
        dialog!!.setCancelable(false)
        dialog!!.show()
    }

    override fun onDateSet(
        view: DatePicker,
        year: Int,
        monthOfYear: Int,
        dayOfMonth: Int
    ) {
        _birthYear = year
        _month = monthOfYear
        _day = dayOfMonth
        updateDisplay()
    }

    fun showDialog1() {
        dialog = DatePickerDialog(
            _context, this,
            calendar[Calendar.YEAR], calendar[Calendar.MONTH],
            calendar[Calendar.DAY_OF_MONTH]
        )
        val newCal = Calendar.getInstance()
        newCal[calendar[Calendar.YEAR] - 6, calendar[Calendar.MONTH]] =
            calendar[Calendar.DAY_OF_MONTH]
        dialog!!.datePicker.maxDate = newCal.timeInMillis
        dialog!!.setCancelable(false)
        dialog!!.show()
    }

    fun showDialog2() {
        dialog = DatePickerDialog(
            _context, this,
            calendar[Calendar.YEAR], calendar[Calendar.MONTH],
            calendar[Calendar.DAY_OF_MONTH]
        )
        dialog!!.setCancelable(false)
        dialog!!.show()
    }

    fun showDialog3() {
        dialog = DatePickerDialog(
            _context, this,
            calendar[Calendar.YEAR], calendar[Calendar.MONTH],
            calendar[Calendar.DAY_OF_MONTH]
        )
        dialog!!.datePicker.maxDate = Date().time
        dialog!!.setCancelable(false)
        dialog!!.show()
    }

    fun showDialogFor18Yrs() {
        dialog = DatePickerDialog(
            _context, this,
            calendar[Calendar.YEAR], calendar[Calendar.MONTH],
            calendar[Calendar.DAY_OF_MONTH]
        )
        val newCal = Calendar.getInstance()
        newCal[calendar[Calendar.YEAR] - 2, calendar[Calendar.MONTH]] =
            calendar[Calendar.DAY_OF_MONTH]
        dialog!!.datePicker.maxDate = newCal.timeInMillis
        dialog!!.setCancelable(false)
        dialog!!.show()
    }

    private fun updateDisplay() {
        val mDay: String = if (_day >= 10) _day.toString() else "0$_day"
        val mMonth: String = if (_month + 1 >= 10) (_month + 1).toString() else "0" + (_month + 1)
        val myFormat = "dd-MM-yyyy"
        val formatter =
            SimpleDateFormat(myFormat, Locale.US)
        val date = formatter.parse(
            StringBuilder() // Month is 0 based so add 1
                .append(mDay).append("-").append(mMonth).append("-").append(_birthYear).append(" ")
                .toString()
        )
        _editText.setText(
            formatter.format(date!!)
        )
        _editText.isFocusable = true
        _editText.setSelection(_editText.text.length - 1)
    }
}