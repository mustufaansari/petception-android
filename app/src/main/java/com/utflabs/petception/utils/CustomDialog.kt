package com.utflabs.petception.utils

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.DrawableRes
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import com.utflabs.petception.R
import com.utflabs.petception.databinding.DialogLayoutBinding
import com.utflabs.petception.listeners.DialogListeners

import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.math.NumberUtils

/**
 * created by Mustufa Ansari on 18,July,2020
 * Email : mustufaayub82@gmail.com
 */
class CustomDialog private constructor(
    private var context: Context,
    private var title: String,
    private var description: String,
    @DrawableRes private var drawableResourceId: Int,
    @LayoutRes private var layoutResourceId: Int,
    private var positiveButton: String,
    private var negativeButton: String,
    private var callback: DialogListeners?,
    private var isCancelable: Boolean
) {
    fun buildDialog(): Dialog {
        val dialog = Dialog(context, R.style.customDialog)
        dialog.setCancelable(isCancelable)

        val view: View = LayoutInflater.from(context).inflate(R.layout.dialog_layout, null, false)
        val binding: DialogLayoutBinding = DialogLayoutBinding.bind(view)
        dialog.setContentView(binding.root)

        if (drawableResourceId != NumberUtils.INTEGER_ZERO) {
            binding.iVLogo.visibility = View.VISIBLE
            binding.iVLogo.setImageDrawable(ContextCompat.getDrawable(context, drawableResourceId))
        } else {
            binding.iVLogo.visibility = View.GONE
        }

        if (title.isNotEmpty()) {
            binding.tVTitle.visibility = View.VISIBLE
            binding.tVTitle.text = title
        } else {
            binding.tVTitle.visibility = View.GONE
        }

        if (description.isNotEmpty()) {
            binding.tVDescription.visibility = View.VISIBLE
            binding.tVDescription.text = description
        } else {
            binding.tVDescription.visibility = View.GONE
        }

        if (layoutResourceId != NumberUtils.INTEGER_ZERO) {
            binding.viewStub.viewStub?.layoutResource = layoutResourceId
            binding.viewStub.viewStub?.inflate()
        } else {
            binding.viewStub.viewStub?.visibility = View.GONE
        }

        if (positiveButton.isNotEmpty()) {
            binding.btnPositive.visibility = View.VISIBLE
            binding.btnPositive.text = positiveButton
            binding.btnPositive.setOnClickListener {
                callback?.let { callback!!.onPositiveButtonTap() } ?: run { dialog.dismiss() }
            }
        } else {
            binding.btnPositive.visibility = View.GONE
        }

        if (negativeButton.isNotEmpty()) {
            binding.btnNegative.visibility = View.VISIBLE
            binding.btnNegative.text = negativeButton
            binding.btnNegative.setOnClickListener {
                callback?.let { callback!!.onNegativeButtonTap() } ?: run { dialog.dismiss() }
            }
        } else {
            binding.btnNegative.visibility = View.GONE
        }

        return dialog
    }

    internal class Builder(private var context: Context) {
        private var title: String = StringUtils.EMPTY
        private var description: String = StringUtils.EMPTY

        @DrawableRes
        private var drawableResourceId: Int = NumberUtils.INTEGER_ZERO

        @LayoutRes
        private var layoutResourceId: Int = NumberUtils.INTEGER_ZERO
        private var positiveButton: String = StringUtils.EMPTY
        private var negativeButton: String = StringUtils.EMPTY
        private var callback: DialogListeners? = null
        private var isCancelable: Boolean = false

        /**
         * Dialog Set Title
         * @param title : Title text
         */
        fun setTitle(title: String): Builder {
            this.title = title
            return this
        }

        /**
         * Dialog Set Description
         * @param description : Description text
         */
        fun setDescription(description: String): Builder {
            this.description = description
            return this
        }

        /**
         * Dialog Set Drawable Resource Id
         * @param drawableResourceId: Drawable Id to set on top of dialog
         */
        fun setDrawableResourceId(drawableResourceId: Int): Builder {
            this.drawableResourceId = drawableResourceId
            return this
        }

        /**
         * Dialog Set Layout Resource Id
         * @param layoutResourceId : Layout Id to inflate in view stub
         */
        fun setLayoutResourceId(layoutResourceId: Int): Builder {
            this.layoutResourceId = layoutResourceId
            return this
        }

        /**
         * Dialog Set Positive Text
         * @param positiveButton : Button text
         */
        fun setPositiveButton(positiveButton: String): Builder {
            this.positiveButton = positiveButton
            return this
        }

        /**
         * Dialog Set Negative Text
         * @param negativeButton : Button text
         */
        fun setNegativeButton(negativeButton: String): Builder {
            this.negativeButton = negativeButton
            return this
        }

        /**
         * Dialog Callback
         * If callback is not provided and button fields text are provided
         * then on each button the dialog will be closed or dismiss.
         * @param callback : Dialog Listeners callback
         */
        fun setCallback(callback: DialogListeners): Builder {
            this.callback = callback
            return this
        }

        /**
         * Dialog Set Cancelable
         * By default cancelable is false
         * @param isCancelable : True or False
         */
        fun setCancelable(isCancelable: Boolean): Builder {
            this.isCancelable = isCancelable
            return this
        }

        fun build(): Dialog = CustomDialog(
            context,
            title,
            description,
            drawableResourceId,
            layoutResourceId,
            positiveButton,
            negativeButton,
            callback,
            isCancelable
        ).buildDialog()
    }

}