package com.utflabs.petception.utils

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.Color
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.util.Log
import android.view.View
import com.facebook.AccessToken
import com.facebook.Profile
import com.google.firebase.iid.FirebaseInstanceId
import com.petception.base.storage.AppPreferences
import com.petception.base.utils.InjectUtils
import com.utflabs.petception.listeners.DialogListeners
import org.apache.commons.lang3.StringUtils


/**
 * Created by Mustufa Ansari on 13/10/2020.
 * Copyright (c) 2020 All rights reserved.
 */
object Utils {

    enum class StreamStatus {
        IS_LIVE_FB, IS_LIVE_YT
    }


    private const val marshmallowMacAddress = "02:00:00:00:00:00"
    private const val fileAddressMac = "/sys/class/net/wlan0/address"

    /**
     * requesting for FCM Id
     */
    fun requestFCMID() {
        try {
            FirebaseInstanceId.getInstance()
                .instanceId.addOnSuccessListener {
                    saveFCMIDtoPreference(it.token)
                }
        } catch (e: java.lang.Exception) {
            Log.v(Utils::class.java.simpleName, e.message!!)
        }
    }

    /**
     *  saving FCM ID into shared pref
     */
    private fun saveFCMIDtoPreference(fcmId: String?) {
        if (!fcmId.isNullOrEmpty()) {
            AppPreferences.fcmRegistrationId = fcmId
            Log.i("FCM_TOKEN", fcmId)
        }
    }

    fun getChannelID(soundUri: Uri?): String {
        var chanelId = StringUtils.EMPTY
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager =
                InjectUtils.appContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val audioAttributes = AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .setUsage(AudioAttributes.USAGE_ALARM)
                .build()
            val importance = NotificationManager.IMPORTANCE_HIGH
            val notificationChannel =
                NotificationChannel(
                    Constants.Notifications.CHANNEL_ID,
                    Constants.Notifications.CHANNEL_NAME,
                    importance
                )
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.RED
            notificationChannel.enableVibration(true)
            notificationChannel.setSound(soundUri, audioAttributes)
            notificationChannel.vibrationPattern = longArrayOf(
                100,
                200,
                300,
                400,
                500,
                400,
                300,
                200,
                400
            )
            notificationManager.createNotificationChannel(notificationChannel)
            chanelId = notificationChannel.id
        }
        return chanelId
    }

    fun getChannelIDForOnGoingNotification(context: Context): String {
        var chanelId = "petception_channel_id_for_live"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            val channelName = "Petception"
            val importance = NotificationManager.IMPORTANCE_LOW
            val notificationChannel = NotificationChannel(chanelId, channelName, importance)
            notificationChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            notificationManager.createNotificationChannel(notificationChannel)
            chanelId = notificationChannel.id
        }
        return chanelId
    }

    fun hideAndShowLayout(isShow: Boolean, layout: View) {
        if (isShow) {
            layout.visibility = View.VISIBLE
        } else {
            layout.visibility = View.GONE
        }
    }

    fun showAlertDialog(context: Context, description: String, title: String, buttonText: String) {
        CustomDialog.Builder(context)
            .setDescription(description)
            .setTitle(title)
            .setPositiveButton(buttonText)
            .build().show()

    }

    fun getFacebookAccessToken(): AccessToken {
        return AccessToken.getCurrentAccessToken()
    }

    fun getFacebookProfile(): Profile = Profile.getCurrentProfile()


    enum class FacebookLiveMode {
        ON_PROFILE, ON_PAGE
    }

    fun getVersion(): String? {
        var currentVersion = StringUtils.EMPTY
        val pm: PackageManager = InjectUtils.appContext.packageManager
        val pInfo: PackageInfo
        try {
            pInfo = pm.getPackageInfo(InjectUtils.appContext.packageName, 0)
            currentVersion = pInfo.versionName
        } catch (e1: PackageManager.NameNotFoundException) {
            e1.printStackTrace()
        }
        return currentVersion
    }
}