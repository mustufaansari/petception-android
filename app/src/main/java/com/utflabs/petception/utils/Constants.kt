package com.utflabs.petception.utils

/**
 * created by Mustufa Ansari on 04,October,2020
 * Email : mustufaayub82@gmail.com
 */
object Constants {


    const val RTMP_URL = "RTMP_URL"
    const val DEVICE_ID = "DEVICE_ID"
    const val REQUEST_YOUTUBE_ACCESS = 100
    const val REQUEST_ACCOUNT_PICKER = 200
    const val PREVIEW_ACTIVITY_CODE = 300

    object DIGITS {
        const val DIGIT_ZERO = 0
        const val DIGIT_ONE = 1
        const val DIGIT_TWO = 2
        const val DIGIT_THREE = 3
        const val DIGIT_FOUR = 4
        const val DIGIT_FIVE = 5
        const val DIGIT_SIX = 6
        const val DIGIT_EIGHT = 8
        const val DIGIT_TEN = 10
        const val DIGIT_HUNDRED = 100
        const val DIGIT_THOUSAND = 1000
        const val DIGIT_TWO_THOUSAND = 2000
    }

    object Actions {
        val STARTFOREGROUND_ACTION = "STARTFOREGROUND_ACTION"
        val STOPFOREGROUND_ACTION = "STOPFOREGROUND_ACTION"
        val ON_CONECTIVITY_CHANGED = "android.net.conn.CONNECTIVITY_CHANGE"
    }

    object Notifications {

        const val CHANNEL_ID = "petception_channel_id"
        const val CHANNEL_NAME = "PetCeption Notifications"
        val NOTIFICATION_CONTENT_TITLE = "PetCeption"
        const val EVENT = "event"
        const val TITLE = "title"
        const val BODY = "body"
        const val SLASH = "/"
        const val DOUBLE_SLASH = "://"
        const val DEVICE_ID = "://"
    }

    /**
     * Inner class for API Status Codes Constants
     */
    internal object ApiStatusCode {
        const val REQUEST_RESULT_OK = "OK"
        const val OK = 200
        const val CHECK_SUB_CODE = 422
        const val UNAUTHORIZED = 401
        const val INTERNAL_SERVER_ERROR = 500
        const val ERROR_PLEASE_TRY_AGAIN = "Something went wrong. Please try again later."
        const val INVALID_USERNAME_PASSWORD = "Something went wrong. Please try again later."
        const val CONNECTION_ERROR_MSG =
            "Problem in internet connectivity. Check your internet connection."
    }

    object SocialLogin {
        const val GOOGLE_LOGIN_REQUEST_CODE = 100
    }

    object IntentKeys {
        const val GO_LIVE_INTENT_KEY = "GO_LIVE_INTENT_KEY"
        const val DEVICE_ID = "DEVICE_ID"
        const val VIDEO_TYPE = "VIDEO_TYPE"
        const val TYPE = "TYPE"
        const val FACEBOOK = "FACEBOOK"
        const val YOUTUBE = "YOUTUBE"
    }

}