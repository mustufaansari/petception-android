package com.utflabs.petception.ui

import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.lifecycle.Observer
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoDevice
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ChallengeContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.MultiFactorAuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler
import com.petception.base.model.response.Wifi
import com.petception.base.networking.ResponseStatus
import com.utflabs.petception.R
import com.utflabs.petception.extensions.gotoActivityWithNoHistory
import com.utflabs.petception.extensions.obtainViewModel
import com.utflabs.petception.viewmodels.AuthViewModel
import com.petception.base.storage.AppPreferences
import com.petception.base.utils.InjectUtils
import com.utflabs.petception.broadcast.InternetConnectivityListener
import com.utflabs.petception.utils.Constants.Notifications.DEVICE_ID
import com.utflabs.petception.utils.Constants.Notifications.EVENT
import com.utflabs.petception.utils.Utils.requestFCMID
import kotlinx.android.synthetic.main.activity_splash.*
import org.apache.commons.lang3.StringUtils
import java.lang.Exception
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class SplashActivity : BaseActivity(), InternetConnectivityListener.ConnectivityReceiverListener {
    lateinit var viewModel: AuthViewModel
    var deviceId: String = StringUtils.EMPTY
    var event: String = StringUtils.EMPTY
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        viewModel = obtainViewModel(AuthViewModel::class.java)
        requestFCMID()
        getHashKeyForFB()

        if (intent != null && intent.hasExtra(EVENT) && intent.getStringExtra(EVENT) != "" && intent.hasExtra(
                DEVICE_ID
            ) && intent.getStringExtra(DEVICE_ID) != ""
        ) {
            deviceId = intent.getStringExtra(DEVICE_ID)!!
            event = intent.getStringExtra(EVENT)!!
        }

        Handler().postDelayed({
            InjectUtils.getCognito.getUser(AppPreferences.userID).getSession(object :
                AuthenticationHandler {
                override fun onSuccess(
                    userSession: CognitoUserSession?,
                    newDevice: CognitoDevice?
                ) {
                    AppPreferences.accessToken = userSession!!.idToken.jwtToken
                    gotoActivityWithNoHistory(
                        MainActivity::class.java,
                        EVENT,
                        event,
                        DEVICE_ID,
                        deviceId
                    )
                }

                override fun onFailure(exception: Exception?) {
                    gotoActivityWithNoHistory(
                        LoginActivity::class.java,
                        EVENT,
                        event,
                        DEVICE_ID,
                        deviceId
                    )
                }

                override fun getAuthenticationDetails(
                    authenticationContinuation: AuthenticationContinuation?,
                    userId: String?
                ) {
                    gotoActivityWithNoHistory(
                        LoginActivity::class.java,
                        EVENT,
                        event,
                        DEVICE_ID,
                        deviceId
                    )
                }

                override fun authenticationChallenge(continuation: ChallengeContinuation?) {}
                override fun getMFACode(continuation: MultiFactorAuthenticationContinuation?) {}
            })
        }, 2000)
    }


    override fun onNetworkConnectionChanged(wifi: Wifi) {

    }

    /** getting hash key
     * for facebook sign in
     */
    private fun getHashKeyForFB() {

        try {
            var info: PackageInfo =
                packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {

                var md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())

                var hashKey: String = String(Base64.encode(md.digest(), 0))

                Log.e("hashKey", ":" + hashKey)
            }

        } catch (e: NoSuchAlgorithmException) {
            Log.e("hashKey", ":", e)
        } catch (e: Exception) {
            Log.e("hashKey", ":", e)
        }

    }
}