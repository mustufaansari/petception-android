package com.utflabs.petception.ui

import android.os.Bundle
import android.view.MenuItem
import com.utflabs.petception.R
import com.utflabs.petception.databinding.ActivityAllDevicesBinding
import com.utflabs.petception.extensions.obtainViewModel
import com.utflabs.petception.viewmodels.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_social_login.*

class AllDevicesActivity : BaseActivity() {

    lateinit var binding: ActivityAllDevicesBinding
    lateinit var viewModel: MainActivityViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_devices)
        viewModel = obtainViewModel(MainActivityViewModel::class.java)


        initToolBar()


    }

    private fun initToolBar() {
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Added Devices"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}