package com.utflabs.petception.ui

import android.content.IntentFilter
import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.petception.base.model.response.Wifi
import com.petception.base.networking.ResponseStatus
import com.petception.base.storage.AppPreferences
import com.petception.base.utils.AppToast.showToast
import com.utflabs.petception.PetceptionApp
import com.utflabs.petception.broadcast.InternetConnectivityListener
import com.utflabs.petception.extensions.obtainViewModel
import com.utflabs.petception.utils.Constants.Actions.ON_CONECTIVITY_CHANGED
import com.utflabs.petception.utils.DialogUtils.hideProgressDialog
import com.utflabs.petception.utils.DialogUtils.showProgressDialog
import com.utflabs.petception.utils.Utils
import com.utflabs.petception.viewmodels.MainActivityViewModel

/**
 * created by Mustufa Ansari on 04,October,2020
 * Email : mustufaayub82@gmail.com
 */
open class BaseActivity : AppCompatActivity(),
    InternetConnectivityListener.ConnectivityReceiverListener {

    private var connectivityListener: InternetConnectivityListener? = null
    var isInternetConnected: Boolean = false
    var wifiModel: Wifi? = null
    lateinit var baseViewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        baseViewModel = obtainViewModel(MainActivityViewModel::class.java)

        connectivityListener =
            InternetConnectivityListener()
        val intentFilterNetwork = IntentFilter()
        intentFilterNetwork.addAction(ON_CONECTIVITY_CHANGED)
        registerReceiver(connectivityListener, intentFilterNetwork)


        baseViewModel.isConnectedToMQTT.observe(this, Observer {
            it?.let {
                when (it.status) {
                    ResponseStatus.SUCCESS -> {

                    }
                    ResponseStatus.LOADING -> {

                    }
                    ResponseStatus.ERROR -> {

                    }
                }
            }
        })


        baseViewModel.isDeviceAdded.observe(this, Observer {
            it?.let {
                when (it.status) {
                    ResponseStatus.ERROR -> {
                    }
                    ResponseStatus.LOADING -> {

                    }
                    ResponseStatus.SUCCESS -> {
                        showToast(it.data!!.message!!)
                        AppPreferences.wifiInfo = null
                    }
                }
            }
        })

    }

    override fun onNetworkConnectionChanged(wifi: Wifi) {
        wifiModel = Wifi(
            wifi.ssId,
            wifi.bssId,
            wifi.isConnectedToWIFI,
            wifi.isConnectedToMobile
        )
        isInternetConnected = wifi.isConnectedToWIFI!!
    }


    fun showProgressLoader() {
        showProgressDialog(this)
    }

    fun hideProgressLoader() {
        hideProgressDialog()
    }

    override fun onStart() {
        super.onStart()
        baseViewModel.connectMQTTClient()
    }

    override fun onResume() {
        super.onResume()
        PetceptionApp.application?.setConnectivityListener(this)

    }


    override fun onDestroy() {
        super.onDestroy()
        connectivityListener?.let {
            unregisterReceiver(it)
        }
    }
}