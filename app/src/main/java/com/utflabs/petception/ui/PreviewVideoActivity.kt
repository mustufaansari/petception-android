package com.utflabs.petception.ui

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.ext.rtmp.RtmpDataSourceFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.*
import com.google.android.exoplayer2.upstream.BandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.petception.base.networking.ResponseStatus
import com.petception.base.storage.AppPreferences
import com.petception.base.utils.AppToast
import com.utflabs.petception.R
import com.utflabs.petception.databinding.ActivityPreviewVideoBinding
import com.utflabs.petception.extensions.obtainViewModel
import com.utflabs.petception.listeners.GenericListeners
import com.utflabs.petception.utils.Constants
import com.utflabs.petception.utils.Constants.DEVICE_ID
import com.utflabs.petception.utils.Constants.IntentKeys.VIDEO_TYPE
import com.utflabs.petception.utils.Constants.RTMP_URL
import com.utflabs.petception.viewmodels.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_preview_video.*
import org.apache.commons.lang3.StringUtils
import org.json.JSONObject


class PreviewVideoActivity : AppCompatActivity(), Player.EventListener {

    private lateinit var binding: ActivityPreviewVideoBinding
    var simpleExoPlayer: SimpleExoPlayer? = null
    private var rtmpUrl: String = StringUtils.EMPTY
    private var deviceId: String = StringUtils.EMPTY
    private var videoType: String = StringUtils.EMPTY

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPreviewVideoBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initToolBar()

        AppPreferences.isStreaming = true

        if (intent != null && intent.extras != null) {
            rtmpUrl = intent.getStringExtra(RTMP_URL) as String
            deviceId = intent.getStringExtra(DEVICE_ID) as String
            Log.d("TestTag", "Device ID $deviceId")
            initializePlayer()
            buildMediaSource(rtmpUrl)
        }

        binding.listener = object : GenericListeners {
            override fun onTapFacebookLive() {
                val returnIntent = Intent()
                returnIntent.putExtra(Constants.IntentKeys.TYPE, Constants.IntentKeys.FACEBOOK)
                returnIntent.putExtra(DEVICE_ID, deviceId)
                setResult(RESULT_OK, returnIntent)
                finish()
            }

            override fun onTapYoutubeLive() {
                val returnIntent = Intent()
                returnIntent.putExtra(Constants.IntentKeys.TYPE, Constants.IntentKeys.YOUTUBE)
                returnIntent.putExtra(DEVICE_ID, deviceId)
                setResult(RESULT_OK, returnIntent)
                finish()
            }
        }

    }


    private fun initializePlayer() {
        if (simpleExoPlayer == null) {
            val bandwidthMeter: BandwidthMeter = DefaultBandwidthMeter()
            val videoTrackSelectionFactory: TrackSelection.Factory =
                AdaptiveTrackSelection.Factory(bandwidthMeter)
            val trackSelector: TrackSelector = DefaultTrackSelector(videoTrackSelectionFactory)
            simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector)
            exoPlayer.player = simpleExoPlayer
        }
    }

    private fun buildMediaSource(rtmpUrl: String) {
        val rtmpDataSourceFactory = RtmpDataSourceFactory()
        val videoSource: MediaSource = ExtractorMediaSource.Factory(rtmpDataSourceFactory)
            .createMediaSource(Uri.parse(rtmpUrl))

        simpleExoPlayer?.prepare(videoSource)
        simpleExoPlayer?.playWhenReady = true
        simpleExoPlayer?.addListener(this)
    }

    private fun initToolBar() {
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Preview Video"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }


    private fun releasePlayer() {
        if (simpleExoPlayer != null) {
            simpleExoPlayer?.release()
            simpleExoPlayer = null
        }
    }

    private fun pausePlayer() {
        if (simpleExoPlayer != null) {
            simpleExoPlayer?.playWhenReady = false
            simpleExoPlayer?.playbackState
        }
    }

    private fun resumePlayer() {
        if (simpleExoPlayer != null) {
            simpleExoPlayer?.playWhenReady = true
            simpleExoPlayer?.playbackState
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters?) {}
    override fun onSeekProcessed() {}
    override fun onTracksChanged(
        trackGroups: TrackGroupArray?,
        trackSelections: TrackSelectionArray?
    ) {
    }

    override fun onPlayerError(error: ExoPlaybackException?) {}
    override fun onLoadingChanged(isLoading: Boolean) {}
    override fun onPositionDiscontinuity(reason: Int) {}
    override fun onRepeatModeChanged(repeatMode: Int) {}
    override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {}
    override fun onTimelineChanged(timeline: Timeline?, manifest: Any?, reason: Int) {}
    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
        when (playbackState) {
            Player.STATE_BUFFERING -> {
                progressBar.visibility = View.VISIBLE
            }
            Player.STATE_ENDED -> {

            }
            Player.STATE_READY -> {
                progressBar.visibility = View.GONE

            }
            else -> {

            }
        }
    }

    override fun onPause() {
        super.onPause()
        pausePlayer()

    }

    override fun onRestart() {
        super.onRestart()
        resumePlayer()

    }

    override fun onResume() {
        super.onResume()
        resumePlayer()

    }

    override fun onDestroy() {
        super.onDestroy()
        releasePlayer()
        AppPreferences.isStreaming = false

    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_OK)
        finish()
    }
}