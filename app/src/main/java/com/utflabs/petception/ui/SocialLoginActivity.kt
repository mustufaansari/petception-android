package com.utflabs.petception.ui

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.databinding.DataBindingUtil
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.utflabs.petception.R
import com.utflabs.petception.databinding.ActivitySocialLoginBinding
import com.utflabs.petception.listeners.GenericListeners
import com.utflabs.petception.utils.Constants.SocialLogin.GOOGLE_LOGIN_REQUEST_CODE
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.Scopes
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.Scope
import com.google.api.services.youtube.YouTube
import com.google.api.services.youtube.YouTubeScopes
import kotlinx.android.synthetic.main.activity_social_login.*
import org.apache.commons.lang3.StringUtils

class SocialLoginActivity : BaseActivity() {

    lateinit var binding: ActivitySocialLoginBinding
    private lateinit var googleSignInClient: GoogleSignInClient
    private lateinit var callbackManager: CallbackManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_social_login)
        initToolBar()
        initGoogleLogin()
        initFacebookLogin()


        /**
         * facebook login callback register
         */
        LoginManager.getInstance().registerCallback(callbackManager,
            object : FacebookCallback<LoginResult?> {
                override fun onSuccess(loginResult: LoginResult?) {
                    updateUIForFacebookLogin()
                }

                override fun onCancel() {
                }

                override fun onError(exception: FacebookException) {
                }
            })



        binding.listeners = object : GenericListeners {
            override fun onTapFacebookConnect() {
                if (wifiModel!!.isConnectedToMobile!! || wifiModel!!.isConnectedToWIFI!!)
                    initiateFacebookLogin()
            }

            override fun onTapGoogleConnect() {
                if (wifiModel!!.isConnectedToMobile!! || wifiModel!!.isConnectedToWIFI!!)
                    initiateGoogleLogin()
            }

            override fun onTapDisConnectFacebookAccount() {
                if (wifiModel!!.isConnectedToMobile!! || wifiModel!!.isConnectedToWIFI!!)
                    logoutFacebookAccount()
            }

            override fun onTapDisConnectGoogleAccount() {
                if (wifiModel!!.isConnectedToMobile!! || wifiModel!!.isConnectedToWIFI!!)
                    logoutGoogleAccount()

            }
        }


    }

    private fun logoutGoogleAccount() {
        googleSignInClient.signOut()
            .addOnSuccessListener {
                updateUIForGoogleLogin(null)
            }
    }

    private fun logoutFacebookAccount() {
        LoginManager.getInstance().logOut()
        updateUIForFacebookLogin()
    }

    private fun initiateFacebookLogin() {
        LoginManager.getInstance()
            .logInWithReadPermissions(
                this, listOf(
                    "public_profile",
                    "pages_show_list",
                    "pages_manage_posts",
                    "pages_read_engagement"
                )
            )
        LoginManager.getInstance()
            .logInWithPublishPermissions(this, listOf("publish_video"))


    }

    private fun initFacebookLogin() {
        callbackManager = CallbackManager.Factory.create()
    }

    private fun initToolBar() {
        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.social_screen_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun initiateGoogleLogin() {
        val intent = googleSignInClient.signInIntent
        startActivityForResult(intent, GOOGLE_LOGIN_REQUEST_CODE)
    }

    private fun initGoogleLogin() {
        val gso =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()
        googleSignInClient = GoogleSignIn.getClient(this, gso)


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GOOGLE_LOGIN_REQUEST_CODE) {
            val signInResult = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = signInResult?.getResult(ApiException::class.java)
                updateUIForGoogleLogin(account)
            } catch (e: Exception) {
                updateUIForGoogleLogin(null)
            }

        }
    }

    private fun updateUIForGoogleLogin(account: GoogleSignInAccount?) {
        account?.let {
            googleLoginButtonText.text = account.email
            connectGoogleButton.visibility = View.GONE
            disconnectGoogleButton.visibility = View.VISIBLE
        } ?: run {
            googleLoginButtonText.text = getString(R.string.connect_google)
            connectGoogleButton.visibility = View.VISIBLE
            disconnectGoogleButton.visibility = View.GONE
        }
    }

    private fun updateUIForFacebookLogin() {
        val profile = Profile.getCurrentProfile()
        profile?.let {
            facebookLoginButtonText.text =
                profile.firstName.plus(StringUtils.SPACE).plus(profile.lastName)
            connectFacebookButton.visibility = View.GONE
            disconnectFacebookButton.visibility = View.VISIBLE
        } ?: kotlin.run {
            facebookLoginButtonText.text = getString(R.string.connect_facebook)
            connectFacebookButton.visibility = View.VISIBLE
            disconnectFacebookButton.visibility = View.GONE

        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onStart() {
        super.onStart()
        val account = GoogleSignIn.getLastSignedInAccount(this)
        updateUIForGoogleLogin(account)
        val facebookAccessToken = AccessToken.getCurrentAccessToken()
        if (facebookAccessToken != null && !facebookAccessToken.isExpired) {
            updateUIForFacebookLogin()
        }

    }

}