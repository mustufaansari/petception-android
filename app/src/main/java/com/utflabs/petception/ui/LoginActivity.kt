package com.utflabs.petception.ui

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.utflabs.petception.R
import com.utflabs.petception.databinding.ActivityLoginBinding
import com.utflabs.petception.extensions.gotoActivity
import com.utflabs.petception.extensions.gotoActivityWithNoHistory
import com.utflabs.petception.extensions.obtainViewModel
import com.utflabs.petception.listeners.GenericListeners
import com.utflabs.petception.utils.FieldValidators
import com.utflabs.petception.viewmodels.AuthViewModel
import com.petception.base.networking.ResponseStatus
import com.petception.base.utils.AppToast.showToast
import com.utflabs.petception.utils.Constants
import com.utflabs.petception.utils.Constants.Notifications.DEVICE_ID
import com.utflabs.petception.utils.Constants.Notifications.EVENT
import com.utflabs.petception.utils.Utils
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.password
import kotlinx.android.synthetic.main.activity_login.userEmail
import org.apache.commons.lang3.StringUtils

class LoginActivity : BaseActivity() {
    lateinit var binding: ActivityLoginBinding
    lateinit var viewModel: AuthViewModel
    var deviceId: String = StringUtils.EMPTY
    var event: String = StringUtils.EMPTY
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewModel = obtainViewModel(AuthViewModel::class.java)
        setListeners()
        Utils.requestFCMID()

        if (intent != null && intent.hasExtra(EVENT) && intent.getStringExtra(
                EVENT
            ) != "" && intent.hasExtra(
                DEVICE_ID
            ) && intent.getStringExtra(DEVICE_ID) != ""
        ) {
            deviceId = intent.getStringExtra(DEVICE_ID)!!
            event = intent.getStringExtra(EVENT)!!
        }

        viewModel.isUserLoggedIn.observe(this, Observer {
            it?.let {
                when (it.status) {
                    ResponseStatus.SUCCESS -> {
                        gotoActivityWithNoHistory(
                            MainActivity::class.java, EVENT, event, DEVICE_ID, deviceId
                        )
                    }
                    ResponseStatus.LOADING -> {
                        progressBar.visibility = View.VISIBLE
                        loginButton.visibility = View.GONE

                    }
                    ResponseStatus.ERROR -> {
                        showToast(it.message!!)
                        progressBar.visibility = View.GONE
                        loginButton.visibility = View.VISIBLE


                    }
                }
            }
        })


        binding.listeners = object : GenericListeners {
            override fun onTapLogin() {
                if (isValidate()) {
                    viewModel.loginUser(userEmail.text.toString(), password.text.toString())
                }
            }

            override fun onTapSignUp() {
                gotoActivity(SignupActivity::class.java)
            }
        }


        binding.versionTextView.text = "Version: ${Utils.getVersion()}"
    }

    private fun setListeners() {
        userEmail.addTextChangedListener(TextFieldValidation(userEmail))
        password.addTextChangedListener(TextFieldValidation(password))
    }

    inner class TextFieldValidation(private val view: View) : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            when (view.id) {
                R.id.userEmail -> {
                    validateEmail()
                }
                R.id.password -> {
                    validatePassword()
                }
            }
        }

    }


    private fun isValidate(): Boolean =
        validateEmail() && validatePassword()

    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    private fun validatePassword(): Boolean {
        when {
            password.text.toString().trim().isEmpty() -> {
                loginInputLayoutPassword.error = "Required Field!"
                requestFocus(password)
                return false
            }
            password.text.toString().length <= resources.getInteger(R.integer.password_length) -> {
                loginInputLayoutPassword.error = "password can't be less than 6"
                requestFocus(password)
                return false
            }
            else -> {
                loginInputLayoutPassword.isErrorEnabled = false
            }
        }
        return true
    }

    private fun validateEmail(): Boolean {
        if (userEmail.text.toString().trim().isEmpty()) {
            loginInputLayoutEmail.error = "Required Field!"
            requestFocus(userEmail)
            return false
        } else if (!FieldValidators.isValidEmail(userEmail.text.toString())) {
            loginInputLayoutEmail.error = "Invalid Email!"
            requestFocus(userEmail)
            return false
        } else {
            loginInputLayoutEmail.isErrorEnabled = false
        }
        return true
    }

}