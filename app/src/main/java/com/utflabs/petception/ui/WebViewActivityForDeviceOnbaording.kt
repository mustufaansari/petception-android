package com.utflabs.petception.ui

import android.annotation.SuppressLint
import android.net.http.SslError
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.webkit.*
import androidx.appcompat.app.ActionBarDrawerToggle
import com.petception.base.model.response.Wifi
import com.petception.base.storage.AppPreferences
import com.petception.base.utils.RemoteConstant.URL_FOR_ON_BOARDING
import com.utflabs.petception.R
import com.utflabs.petception.extensions.obtainViewModel
import com.utflabs.petception.viewmodels.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_web_view_for_device_onbaording.*
import kotlinx.android.synthetic.main.activity_web_view_for_device_onbaording.toolbar

class WebViewActivityForDeviceOnbaording : BaseActivity() {

    lateinit var viewModel: MainActivityViewModel

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view_for_device_onbaording)
        viewModel = obtainViewModel(MainActivityViewModel::class.java)

        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.device_on_boarding)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        webView.settings.javaScriptEnabled = true
        webView.webViewClient = webViewClient
        webView.loadUrl(URL_FOR_ON_BOARDING)
    }
    override fun onNetworkConnectionChanged(wifi: Wifi) {

    }
    private val webViewClient: WebViewClient = object : WebViewClient() {
        override fun onPageFinished(view: WebView?, url: String?) {
        }

        override fun onReceivedError(
            view: WebView?,
            request: WebResourceRequest?,
            error: WebResourceError?
        ) {
        }

        override fun onReceivedSslError(
            view: WebView?,
            handler: SslErrorHandler?,
            error: SslError?
        ) {
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}