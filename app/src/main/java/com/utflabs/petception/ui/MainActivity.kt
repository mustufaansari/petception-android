package com.utflabs.petception.ui

import android.Manifest
import android.accounts.AccountManager
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import android.widget.Toast
import androidx.annotation.MenuRes
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.facebook.*
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.Scopes
import com.google.android.gms.common.api.Scope
import com.google.android.material.navigation.NavigationView
import com.google.api.client.extensions.android.http.AndroidHttp
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.json.JsonFactory
import com.google.api.client.json.gson.GsonFactory
import com.google.api.client.util.ExponentialBackOff
import com.google.api.services.youtube.YouTubeScopes
import com.google.gson.Gson
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.petception.base.model.response.GetUserDevicesResponse
import com.petception.base.model.response.Wifi
import com.petception.base.networking.ResponseStatus
import com.petception.base.source.RoomRepository
import com.petception.base.storage.AppPreferences
import com.petception.base.storage.room.entites.Device
import com.petception.base.utils.AppToast
import com.petception.base.utils.AppToast.showToast
import com.utflabs.petception.R
import com.utflabs.petception.adapter.LastAdapter
import com.utflabs.petception.broadcast.InternetConnectivityListener
import com.utflabs.petception.databinding.ActivityMainBinding
import com.utflabs.petception.databinding.DialogForFbLiveBinding
import com.utflabs.petception.extensions.*
import com.utflabs.petception.listeners.DialogListeners
import com.utflabs.petception.listeners.GenericListeners
import com.utflabs.petception.models.FacebookPageResponse
import com.utflabs.petception.utils.Constants
import com.utflabs.petception.utils.Constants.PREVIEW_ACTIVITY_CODE
import com.utflabs.petception.utils.Constants.REQUEST_ACCOUNT_PICKER
import com.utflabs.petception.utils.Constants.REQUEST_YOUTUBE_ACCESS
import com.utflabs.petception.utils.CustomDialog
import com.utflabs.petception.utils.Utils
import com.utflabs.petception.utils.Utils.getFacebookAccessToken
import com.utflabs.petception.utils.Utils.getFacebookProfile
import com.utflabs.petception.utils.Utils.hideAndShowLayout
import com.utflabs.petception.utils.Utils.showAlertDialog
import kotlinx.android.synthetic.main.activity_main.*
import org.apache.commons.lang3.StringUtils
import org.json.JSONObject
import java.util.*


class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener,
    InternetConnectivityListener.ConnectivityReceiverListener {

    lateinit var actionBarToggle: ActionBarDrawerToggle
    lateinit var binding: ActivityMainBinding
    lateinit var lastAdapter: LastAdapter<Device>
    lateinit var googleSignInClient: GoogleSignInClient

    var isGoogleLogin: Boolean = false
    var isFacebookLogin: Boolean = false
    var deviceId: String = StringUtils.EMPTY
    var credential: GoogleAccountCredential? = null
    val transport = AndroidHttp.newCompatibleTransport()
    val jsonFactory: JsonFactory = GsonFactory()
    var liveRequestForFB = false
    var liveRequestForYT = false
    var selectedDevice: Pair<String, Boolean>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        /**
         * toolbar initialization
         */
        initToolbar()
        /**
         * setting up recyclerView adapter
         */
        setAdapter()
        /**
         * Getting intent if user coming from notification
         */
        getIntentAndNavigateAccordingly()
        /**
         * Google login setup
         */
        initGoogleLogin()
        /**
         * Fetching All register devices of user response from Local db
         */
        baseViewModel.getAllDevicesList.observe(this, Observer {
            if (it.isNullOrEmpty()) {
                baseViewModel.getAllUserDevices()
            } else {
                progressBar.visibility = View.GONE
                noDevicesFoundText.visibility = View.GONE
                allDevicesList.visibility = View.VISIBLE
                lastAdapter.items = it as ArrayList<Device>
                it.forEach {
                    val jsonObject = JSONObject().apply {
                        put("action", "device_status")
                    }
                    baseViewModel.subscribeToTopic("${it.deviceId}/status", jsonObject)
                }
            }
        })


        /**
         * Observing MQTT communication
         */
        baseViewModel.isConnectedToMQTT.observe(this, Observer {
            it?.let {
                when (it.status) {
                    ResponseStatus.ERROR -> {
                        hideProgressLoader()
                        Log.i("error", it.message!!)
                    }
                    ResponseStatus.LOADING -> {
                    }
                    ResponseStatus.SUCCESS -> {
                        hideProgressLoader()
                        Log.i("preview", it.data!!)
                        handlingResponse(it.data!!)

                    }
                }

            }
        })

        /**
         * Observing Youtube Live streaming
         */
        baseViewModel.youTubeLiveStreaming.observe(this, Observer {
            it?.let {
                when (it.status) {
                    ResponseStatus.SUCCESS -> {
                        hideProgressLoader()
                        if (it.data != null && it.data!!.ingestionAddress != null) {
                            val jsonObject = JSONObject().apply {
                                put("ytkey", it.data!!.ingestionAddress)
                                put("action", "live")
                            }
                            baseViewModel.subscribeToTopic("${deviceId}/live", jsonObject)
                        } else {
                            showToast("Something went wrong! Try again later.")
                        }
                    }
                    ResponseStatus.LOADING -> {
                        showProgressLoader()
                    }
                    ResponseStatus.ERROR -> {
                        hideProgressLoader()
                        showAlertDialog(this, it.message!!, "Alert!", "Okay!")
                    }
                }
            }
        })

        /**
         * Fetching All register devices of user response from API
         */
        baseViewModel.getAllUserDevices.observe(this, Observer {
            it?.let {
                when (it.status) {
                    ResponseStatus.SUCCESS -> {
                        if (it.data != null && !it.data!!.data.isNullOrEmpty()) {
                            baseViewModel.insertDevices(it.data!!.data!!)
                        } else {
                            progressBar.visibility = View.GONE
                            noDevicesFoundText.visibility = View.VISIBLE
                            allDevicesList.visibility = View.GONE
                        }
                    }
                    ResponseStatus.LOADING -> {
                        progressBar.visibility = View.VISIBLE
                        noDevicesFoundText.visibility = View.GONE
                        allDevicesList.visibility = View.GONE

                    }
                    ResponseStatus.ERROR -> {
                        progressBar.visibility = View.GONE
                        noDevicesFoundText.visibility = View.VISIBLE
                        noDevicesFoundText.text = it.message
                        allDevicesList.visibility = View.GONE
                    }
                }
            }
        })


        binding.listener = object : GenericListeners {
            override fun onTapOpenSettingsForPermission() {
                openSettingsIntent()
            }

            override fun onTapOpenSettingsForWifi() {
                openWiFiIntent()
            }

            override fun onTapRefresh() {
                baseViewModel.getAllUserDevices()
            }
        }
    }

    private fun initGoogleLogin() {
        val gso =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()
        googleSignInClient = GoogleSignIn.getClient(this, gso)

    }

    private fun handlingResponse(data: String) {
        val obj = JSONObject(data)
        when {
            obj.has("streaming") && obj.getString("streaming")
                .equals("start", ignoreCase = true) -> {
                showToast("Streaming has been started!")
                val intent = Intent(this, PreviewVideoActivity::class.java)
                intent.putExtra(Constants.RTMP_URL, obj.getString("stream_link"))
                intent.putExtra(Constants.DEVICE_ID, selectedDevice?.first)
                startActivityForResult(intent, PREVIEW_ACTIVITY_CODE)
            }
            obj.has("streaming") && obj.getString("streaming")
                .equals("stop", ignoreCase = true) -> {
                showToast(obj.getString("message"))


            }
            obj.has("error") -> {
                showAlertDialog(
                    this,
                    description = obj.getString("error"),
                    title = "Error!",
                    buttonText = "Ok"
                )
            }
            obj.has("live") -> {
                showToast(obj.getString("live"))
            }
            obj.has("fbstatus") -> {
                val isFbLive = obj.getBoolean("fbstatus")
                val isYtLive = obj.getBoolean("ytstatus")
                when {
                    isFbLive -> {
                        showDialogIfDeviceIsAlreadyStreaming(Utils.StreamStatus.IS_LIVE_FB)
                    }
                    isYtLive -> {
                        showDialogIfDeviceIsAlreadyStreaming(Utils.StreamStatus.IS_LIVE_YT)
                    }
                    liveRequestForYT -> {
                        createLiveVideoRequestForYoutube()
                    }
                    liveRequestForFB -> {
                        createLiveVideoRequestForFacebook()
                    }
                }
            }
            obj.has("message") -> {
                showToast(obj.getString("message"))
            }

            obj.has("status") -> {
                updateAdapter(obj)
            }
        }
    }

    private fun updateAdapter(obj: JSONObject) {
        lastAdapter.itemsFiltered.forEach {
            if (obj.getString("status").contentEquals("1")) {
                it.isDeviceActive = it.deviceId == obj.getString("device_id")
                it.batteryLevel = obj.getString("battery_level") ?: "Device is offline"
            }
        }
        lastAdapter.notifyDataSetChanged()
    }

    private fun createLiveVideoRequestForYoutube() {
        val scope = Scope(YouTubeScopes.YOUTUBE)
        if (!GoogleSignIn.hasPermissions(
                GoogleSignIn.getLastSignedInAccount(this),
                scope
            )
        ) {
            GoogleSignIn.requestPermissions(
                this,
                REQUEST_YOUTUBE_ACCESS,
                GoogleSignIn.getLastSignedInAccount(this),
                scope
            )

        } else {
            baseViewModel.callYoutubeLive(transport, jsonFactory, credential!!)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                REQUEST_YOUTUBE_ACCESS -> {
                    baseViewModel.callYoutubeLive(transport, jsonFactory, credential!!)
                }
                REQUEST_ACCOUNT_PICKER -> {
                    val accountName = data!!.extras!!.getString(
                        AccountManager.KEY_ACCOUNT_NAME
                    )
                    if (accountName != null) {
                        credential!!.selectedAccountName = accountName

                    }
                }
                PREVIEW_ACTIVITY_CODE -> {
                    selectedDevice?.let {
                        val jsonObject = JSONObject().apply {
                            put("action", "stop")
                        }
                        baseViewModel.subscribeToTopic("${it.first}/stop", jsonObject)
                    }

                    data?.let {

                        val type = it.getStringExtra(Constants.IntentKeys.TYPE).toString()
                        val deviceId = it.getStringExtra(Constants.DEVICE_ID).toString()
                        Log.d("ResultData", "Type $type and Device ID $deviceId")

                        if (type.contentEquals(Constants.IntentKeys.FACEBOOK)) {
                            facebookLive(deviceId)

                        }
                        else if (type.contentEquals(Constants.IntentKeys.YOUTUBE)) {
                            youtubeLive(deviceId)
                        }
                    }
                }
            }

        }

    }

    private fun showDialogIfDeviceIsAlreadyStreaming(isLive: Utils.StreamStatus) {
        var dialog: Dialog? = null
        dialog = CustomDialog.Builder(this)
            .setTitle(if (isLive == Utils.StreamStatus.IS_LIVE_FB) "You are Already Live On Facebook" else "You are Already Live On YouTube")
            .setDescription("Would you like to stop it?")
            .setPositiveButton("Yes")
            .setNegativeButton("No")
            .setCallback(object : DialogListeners {
                override fun onPositiveButtonTap() {
                    showProgressLoader()
                    val jsonObject = JSONObject().apply {
                        put("action", "stop")
                    }
                    baseViewModel.subscribeToTopic("${deviceId}/stop", jsonObject)
                    if (isLive == Utils.StreamStatus.IS_LIVE_YT) {
                        baseViewModel.stopYoutubeStreaming(transport, jsonFactory, credential!!)
                        stopPushingLiveServiceOnYouTube()
                    }
                    dialog?.dismiss()
                }

                override fun onNegativeButtonTap() {
                    dialog?.dismiss()
                }
            }).build()
        dialog.show()
    }


    /**
     *  action  = preview
     *  for start preview topic --> mac-address/preview
     *  for Stop preview topic --> mac-address/stop
     *  onSubItemOneClick --> for Preview
     *  onSubItemTwoClick --> for Facebook
     *  onSubItemThreeClick --> for YouTube
     * */
    private fun setAdapter() {
        lastAdapter = LastAdapter(R.layout.item_devices_list, object :
            LastAdapter.OnItemClickListener<Device> {
            override fun onSubItemOneClick(item: Device) {
                showProgressLoader()
                val jsonObject = JSONObject().apply {
                    put("action", "preview")
                }
                baseViewModel.subscribeToTopic("${item.deviceId}/preview", jsonObject)
                deviceId = item.deviceId
                selectedDevice = Pair(item.deviceId, !item.isPreviewActive)
            }

            override fun onSubItemTwoClick(item: Device) {
                deviceId = item.deviceId
                facebookLive(item.deviceId)
            }

            override fun onItemClick(item: Device) {

            }

            override fun onSubItemThreeClick(item: Device) {
                deviceId = item.deviceId
                youtubeLive(item.deviceId)
            }

            override fun onSubItemFourClick(item: Device) {
                showProgressLoader()
                val jsonObject = JSONObject().apply {
                    put("action", "stop")
                }
                baseViewModel.subscribeToTopic("${deviceId}/stop", jsonObject)
            }

            override fun onSubItemFiveClick(view: View, item: Device) {
                showPopupMenu(view, R.menu.device_popup_menu, item)
            }
        })
        allDevicesList.adapter = lastAdapter
    }

    private fun facebookLive(deviceId: String){
        if (isFacebookLogin) {
            val jsonObject = JSONObject().apply {
                put("action", "stream_status")
            }
            baseViewModel.subscribeToTopic("${deviceId}/stream_status", jsonObject)
            liveRequestForFB = true
            liveRequestForYT = false
        } else {
            Toast.makeText(
                this@MainActivity,
                "Login with Facebook",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun youtubeLive(deviceId: String) {
        if (isGoogleLogin) {
            val jsonObject = JSONObject().apply {
                put("action", "stream_status")
            }
            baseViewModel.subscribeToTopic("${deviceId}/stream_status", jsonObject)
            liveRequestForYT = true
            liveRequestForFB = false

        } else {
            Toast.makeText(
                this@MainActivity,
                "Login with Google",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun showPopupMenu(
        view: View,
        @MenuRes devicePopupMenu: Int,
        data: Device
    ) {
        val popup = PopupMenu(this, view)
        popup.menuInflater.inflate(devicePopupMenu, popup.menu)

        popup.setOnMenuItemClickListener { item ->
            when (item?.itemId) {
                R.id.shutDownPi -> {
                    showProgressLoader()
                    val jsonObject = JSONObject().apply {
                        put("action", "shutdown")
                    }
                    baseViewModel.subscribeToTopic("${data.deviceId}/shutdown", jsonObject)
                    true
                }
                R.id.wifiReboot -> {
                    showProgressLoader()
                    val jsonObject = JSONObject().apply {
                        put("action", "enableAP")
                    }
                    baseViewModel.subscribeToTopic("${data.deviceId}/wifi", jsonObject)
                    true
                }
                else -> {
                    false
                }
            }
        }


        popup.show()
    }

    private fun createLiveVideoRequestForFacebook() {
        val dialog = Dialog(this, R.style.alertDialogStyle)
        val view: View = LayoutInflater.from(this).inflate(R.layout.dialog_for_fb_live, null, false)
        val binding: DialogForFbLiveBinding = DialogForFbLiveBinding.bind(view)
        dialog.setContentView(binding.root)
        val adapter: LastAdapter<FacebookPageResponse.Data> =
            LastAdapter(R.layout.item_fb_page, object :
                LastAdapter.OnItemClickListener<FacebookPageResponse.Data> {
                override fun onItemClick(item: FacebookPageResponse.Data) {
                    dialog.dismiss()
                    facebookNewPostRequest(
                        Utils.FacebookLiveMode.ON_PAGE,
                        item.id!!,
                        item.accessToken!!
                    )
                }
            })
        dialog.show()

        binding.modeSelectionRadioGroup.setOnCheckedChangeListener { group, checkedId ->
            if (checkedId == R.id.onPage) {
                binding.progressBar.visibility = View.VISIBLE
                val request = GraphRequest.newGraphPathRequest(
                    getFacebookAccessToken(),
                    "/${getFacebookProfile().id}/accounts"
                ) {
                    binding.progressBar.visibility = View.GONE
                    binding.pagesListRecyclerView.visibility = View.VISIBLE
                    val gson = Gson().fromJson(it.rawResponse, FacebookPageResponse::class.java)
                    if (gson != null && !gson.data.isNullOrEmpty()) {
                        binding.goLiveButton.visibility = View.GONE
                        binding.selectPageHeading.visibility = View.VISIBLE
                        adapter.items = gson.data as ArrayList<FacebookPageResponse.Data>
                    } else {
                        binding.noPageFoundPlaceHolder.visibility = View.VISIBLE
                        binding.progressBar.visibility = View.GONE
                        binding.pagesListRecyclerView.visibility = View.GONE
                    }
                }
                request.executeAsync()
            }
            if (checkedId == R.id.onProfile) {
                binding.goLiveButton.visibility = View.VISIBLE
                binding.selectPageHeading.visibility = View.GONE
                binding.noPageFoundPlaceHolder.visibility = View.GONE
                binding.progressBar.visibility = View.GONE
                binding.pagesListRecyclerView.visibility = View.GONE
            }
            binding.pagesListRecyclerView.adapter = adapter
        }

        binding.goLiveButton.setOnClickListener {
            dialog.dismiss()
            facebookNewPostRequest(Utils.FacebookLiveMode.ON_PROFILE)

        }

    }


    private fun facebookNewPostRequest(
        facebookLiveMode: Utils.FacebookLiveMode,
        pageId: String = StringUtils.EMPTY,
        pageAccessToken: String = StringUtils.EMPTY
    ) {
        showProgressLoader()
        val graphPath: String = if (facebookLiveMode == Utils.FacebookLiveMode.ON_PROFILE) {
            "/${getFacebookProfile().id}/live_videos"
        } else {
            "/${pageId}/live_videos"
        }
        val accessToken: AccessToken =
            if (facebookLiveMode == Utils.FacebookLiveMode.ON_PAGE) AccessToken(
                pageAccessToken, getFacebookAccessToken().applicationId,
                getFacebookAccessToken().userId,
                getFacebookAccessToken().permissions,
                getFacebookAccessToken().declinedPermissions,
                null,
                null,
                null,
                null,
                null
            ) else getFacebookAccessToken()

        val request = GraphRequest.newPostRequest(
            accessToken, graphPath,
            JSONObject()
        ) {
            if (it.error == null) {
                val streamUrl = it.jsonObject.getString("secure_stream_url")
                val jsonObject = JSONObject().apply {
                    put("fbkey", streamUrl)
                    put("action", "live")
                }
                baseViewModel.subscribeToTopic("${deviceId}/live", jsonObject)
            } else {
                hideProgressLoader()
            }
        }
        request.executeAsync()
    }

    private fun getIntentAndNavigateAccordingly() {
        if (intent != null && intent.hasExtra(Constants.Notifications.EVENT) && intent.getStringExtra(
                Constants.Notifications.EVENT
            ) != "" && intent.hasExtra(
                Constants.Notifications.DEVICE_ID
            ) && intent.getStringExtra(Constants.Notifications.DEVICE_ID) != ""
        ) {
            val id = intent.getStringExtra(Constants.Notifications.DEVICE_ID)
            val jsonObject = JSONObject().apply {
                put("action", "preview")
            }
            deviceId = id!!
            baseViewModel.subscribeToTopic("${deviceId}/preview", jsonObject)

        }
    }

    /**
     * initiating toolbar and drawer listener
     */
    private fun initToolbar() {
        setSupportActionBar(toolbar)
        actionBarToggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.open_drawer,
            R.string.close_drawer
        )
        drawerLayout.addDrawerListener(actionBarToggle)
        supportActionBar?.title = getString(R.string.welcome_text)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        actionBarToggle.syncState()
        navigation.setNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.socialConnections -> {
                gotoActivity(SocialLoginActivity::class.java)
            }
            R.id.logout -> {
                logoutAllAccounts()
            }
            R.id.deviceOnBoarding -> {
                gotoActivity(WebViewActivityForDeviceOnbaording::class.java)
            }
            R.id.privacyPolicyPage -> {
                gotoActivity(PrivacyPolicyActivity::class.java)
            }
        }
        drawerLayout.closeDrawers()
        return true
    }

    private fun logoutAllAccounts() {
        baseViewModel.logout()
        gotoActivityWithNoHistory(LoginActivity::class.java)
    }

    /**
     * callback triggers when internet is not connected.
     * @param wifi model along with info of connectivity and SSID
     */
    override fun onNetworkConnectionChanged(wifi: Wifi) {
        if (wifi.isConnectedToWIFI!!) {
            hideAndShowLayout(false, binding.wifiErrorLayout.wifiErrorRootLayout)
            binding.wifiSSDName.text =
                getString(R.string.connected_to).plus(StringUtils.SPACE).plus(wifi.ssId)
            binding.wifiSSDName.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.green
                )
            )
            if (wifi.ssId.contains("petception", ignoreCase = true)) {
                AppPreferences.wifiInfo = Wifi(wifi.ssId, wifi.bssId, null, null)
            }
            if (AppPreferences.wifiInfo != null && AppPreferences.wifiInfo!!.ssId.isNotEmpty()) {
                baseViewModel.addDevice(
                    AppPreferences.wifiInfo!!.ssId.substring(
                        1,
                        AppPreferences.wifiInfo!!.ssId.length - 1
                    ), AppPreferences.wifiInfo!!.bssId
                )
            }
        } else {
            hideAndShowLayout(true, binding.wifiErrorLayout.wifiErrorRootLayout)
            binding.wifiSSDName.text = getString(R.string.no_connected_to_internet)
            binding.wifiSSDName.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.colorPrimary
                )
            )
        }
    }


    /**
     * checking for wifi location permissions
     * showing error layout if permission is not granted
     */
    private fun askingForPermissions() {
        Dexter.withContext(this)
            .withPermissions(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    report?.let {
                        if (it.areAllPermissionsGranted()) {
                            hideAndShowLayout(
                                false,
                                binding.noPermissionGrantedLayout.locationPermissionRootLayout
                            )
                        }
                        if (it.isAnyPermissionPermanentlyDenied) {
                            hideAndShowLayout(
                                true,
                                binding.noPermissionGrantedLayout.locationPermissionRootLayout
                            )
                        }
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }
            }).withErrorListener {
            }
            .onSameThread()
            .check()

    }


    override fun onStart() {
        super.onStart()
        val account = GoogleSignIn.getLastSignedInAccount(this)
        val facebookAccessToken = AccessToken.getCurrentAccessToken()
        facebookAccessToken?.let {
            isFacebookLogin = true
        } ?: kotlin.run {
            isFacebookLogin = false
        }
        account?.let {
            isGoogleLogin = true
            credential = GoogleAccountCredential.usingOAuth2(
                this, listOf(Scopes.PROFILE, YouTubeScopes.YOUTUBE)
            )
            credential?.backOff = ExponentialBackOff()
            credential?.selectedAccountName = account.account?.name
        } ?: kotlin.run {
            isGoogleLogin = false
        }
    }

    override fun onResume() {
        super.onResume()
        askingForPermissions()
    }


}