package com.utflabs.petception.ui

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.utflabs.petception.R
import com.utflabs.petception.databinding.ActivitySignupBinding
import com.utflabs.petception.extensions.gotoActivity
import com.utflabs.petception.extensions.gotoActivityWithNoHistory
import com.utflabs.petception.extensions.obtainViewModel
import com.utflabs.petception.helper.DatePHelper
import com.utflabs.petception.listeners.DialogListeners
import com.utflabs.petception.listeners.GenericListeners
import com.utflabs.petception.utils.CustomDialog
import com.utflabs.petception.utils.FieldValidators
import com.utflabs.petception.utils.FieldValidators.isStringContainNumber
import com.utflabs.petception.utils.FieldValidators.isStringContainSpecialCharacter
import com.utflabs.petception.utils.FieldValidators.isStringLowerAndUpperCase
import com.utflabs.petception.viewmodels.AuthViewModel
import com.petception.base.networking.ResponseStatus
import com.petception.base.utils.AppToast.showToast
import kotlinx.android.synthetic.main.activity_signup.*


class SignupActivity : BaseActivity() {
    lateinit var binding: ActivitySignupBinding
    lateinit var viewModel: AuthViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = obtainViewModel(AuthViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signup)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        setListeners()
        binding.listeners = object : GenericListeners {
            override fun onTapLogin() {
                gotoActivity(LoginActivity::class.java)
            }

            override fun onTapDateOfBirth(view: EditText) {
                val helper = DatePHelper(this@SignupActivity, view)
                helper.showDialog3()

            }

            override fun onTapSignUp() {
                if (isValidate()) {
                    viewModel.userSignup(
                        userEmail.text.toString(),
                        password.text.toString(),
                        userDOB.text.toString(),
                        userName.text.toString()
                    )
                }
            }
        }

        viewModel.isUserRegister.observe(this, Observer {
            it?.let {
                when (it.status) {
                    ResponseStatus.ERROR -> {
                        showToast(it.message!!)
                        signupButton.visibility = View.VISIBLE
                        progressBar.visibility = View.GONE

                    }
                    ResponseStatus.LOADING -> {
                        signupButton.visibility = View.GONE
                        progressBar.visibility = View.VISIBLE

                    }
                    ResponseStatus.SUCCESS -> {
                        progressBar.visibility = View.GONE
                        CustomDialog.Builder(this)
                            .setCancelable(false)
                            .setTitle(getString(R.string.welcome_text))
                            .setDescription(getString(R.string.confirm_email_desc))
                            .setDrawableResourceId(R.drawable.ic_smile)
                            .setPositiveButton(getString(R.string.okay))
                            .setCallback(object : DialogListeners {
                                override fun onPositiveButtonTap() {
                                    gotoActivityWithNoHistory(LoginActivity::class.java)
                                }
                            }).build().show()


                    }
                }
            }
        })
    }

    /**
     * setting listeners for [EditText]
     */
    private fun setListeners() {
        userName.addTextChangedListener(TextFieldValidation(userName))
        userEmail.addTextChangedListener(TextFieldValidation(userEmail))
        userDOB.addTextChangedListener(TextFieldValidation(userDOB))
        password.addTextChangedListener(TextFieldValidation(password))
        confrmPassword.addTextChangedListener(TextFieldValidation(confrmPassword))
    }


    private fun isValidate(): Boolean =
        validateUserName() && validateEmail() && validateDob() && validatePassword() && validateConfirmPassword()

    private fun validateUserName(): Boolean {
        if (userName.text.toString().trim().isEmpty()) {
            inputLayoutUserName.error = "Required Field!"
            userName.requestFocus()
            return false
        } else {
            inputLayoutUserName.isErrorEnabled = false
        }
        return true
    }

    private fun validateDob(): Boolean {
        if (userDOB.text.toString().trim().isEmpty()) {
            textInputLayoutDob.error = "Required Field!"
            userDOB.requestFocus()
            return false
        } else {
            textInputLayoutDob.isErrorEnabled = false
        }
        return true
    }

    private fun validatePassword(): Boolean {
        if (password.text.toString().trim().isEmpty()) {
            inputLayoutPassword.error = "Required Field!"
            password.requestFocus()
            return false
        } else if (password.text.toString().length < 8) {
            inputLayoutPassword.error = "password can't be less than 8"
            password.requestFocus()
            return false
        } else if (!isStringContainNumber(password.text.toString())) {
            inputLayoutPassword.error = "Required at least 1 digit"
            password.requestFocus()
            return false
        } else if (!isStringLowerAndUpperCase(password.text.toString())) {
            inputLayoutPassword.error = "Password must contain upper and lower case letters"
            password.requestFocus()
            return false
        } else if (!isStringContainSpecialCharacter(password.text.toString())) {
            inputLayoutPassword.error = "1 special character required (except +,-)"
            password.requestFocus()
            return false
        } else if (password.text.toString().contains("+") || password.text.toString()
                .contains("-")
        ) {
            inputLayoutPassword.error = "+/- not allowed!"
            password.requestFocus()
            return false
        } else {
            inputLayoutPassword.isErrorEnabled = false
        }
        return true
    }

    private fun validateConfirmPassword(): Boolean {
        when {
            confrmPassword.text.toString().trim().isEmpty() -> {
                inputLayoutConfirmPassword.error = "Required Field!"
                confrmPassword.requestFocus()
                return false
            }
            password.text.toString() != confrmPassword.text.toString() -> {
                inputLayoutConfirmPassword.error = "Passwords don't match"
                confrmPassword.requestFocus()
                return false
            }
            else -> {
                inputLayoutConfirmPassword.isErrorEnabled = false
            }
        }
        return true
    }

    private fun validateEmail(): Boolean {
        if (userEmail.text.toString().trim().isEmpty()) {
            inputLayoutEmail.error = "Required Field!"
            userEmail.requestFocus()
            return false
        } else if (!FieldValidators.isValidEmail(userEmail.text.toString())) {
            inputLayoutEmail.error = "Invalid Email!"
            userEmail.requestFocus()
            return false
        } else {
            inputLayoutEmail.isErrorEnabled = false
        }
        return true
    }

    inner class TextFieldValidation(private val view: View) : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            when (view.id) {
                R.id.userName -> {
                    validateUserName()
                }
                R.id.userEmail -> {
                    validateEmail()
                }
                R.id.userDOB -> {
                    validateDob()
                }
                R.id.password -> {
                    validatePassword()
                }
                R.id.confrmPassword -> {
                    validateConfirmPassword()
                }
            }
        }

    }


}