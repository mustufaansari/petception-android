package com.utflabs.petception.ui

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.common.Scopes
import com.google.api.client.extensions.android.http.AndroidHttp
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.json.JsonFactory
import com.google.api.client.json.gson.GsonFactory
import com.google.api.client.util.ExponentialBackOff
import com.google.api.services.youtube.YouTubeScopes
import com.petception.base.networking.ResponseStatus
import com.petception.base.utils.AppToast
import com.petception.base.utils.AppToast.showToast
import com.utflabs.petception.R
import com.utflabs.petception.databinding.DialogPostCreationBinding
import com.utflabs.petception.extensions.gotoActivity
import com.utflabs.petception.listeners.GenericListeners
import com.utflabs.petception.utils.Constants
import com.utflabs.petception.utils.Constants.IntentKeys.DEVICE_ID
import com.utflabs.petception.utils.Constants.IntentKeys.GO_LIVE_INTENT_KEY
import com.utflabs.petception.utils.DialogUtils
import com.utflabs.petception.utils.Utils
import com.utflabs.petception.viewmodels.MainActivityViewModel
import org.json.JSONObject

/**
 * Created by Mustufa Ansari on 21/12/2020.
 * Email : mustufaayub82@gmail.com
 */
class PostCreationDialog : DialogFragment() {

    lateinit var binding: DialogPostCreationBinding
    private var headerName: String? = null
    val transport = AndroidHttp.newCompatibleTransport()
    val jsonFactory: JsonFactory = GsonFactory()
    var credential: GoogleAccountCredential? = null
    var googleSignedAccount: GoogleSignInAccount? = null
    lateinit var viewModel: MainActivityViewModel
    private var deviceId: String? = null


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            headerName = it.getString(GO_LIVE_INTENT_KEY)
            deviceId = it.getString(DEVICE_ID)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.dialog_post_creation, container, false
        )
        viewModel = ViewModelProvider.NewInstanceFactory().create(MainActivityViewModel::class.java)
        binding.headerText.text =
            if (headerName == "yt") "Go Live On YouTube" else "Go Live On Facebook"
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        googleSignedAccount = GoogleSignIn.getLastSignedInAccount(context)

        credential = GoogleAccountCredential.usingOAuth2(
            context, listOf(Scopes.PROFILE, YouTubeScopes.YOUTUBE)
        )
        credential?.selectedAccount = googleSignedAccount?.account
        credential?.backOff = ExponentialBackOff()


        viewModel.youTubeLiveStreaming.observe(viewLifecycleOwner, Observer {
            it?.let {
                when (it.status) {
                    ResponseStatus.SUCCESS -> {
                        Log.i("youtubeSUCCESS", it.data.toString())


                    }
                    ResponseStatus.LOADING -> {
                    }
                    ResponseStatus.ERROR -> {
                        showToast(it.message!!)

                    }
                }
            }
        })



        viewModel.isConnectedToMQTT.observe(viewLifecycleOwner, Observer {
            it?.let {
                when (it.status) {
                    ResponseStatus.ERROR -> {
                        DialogUtils.hideProgressDialog()
                        Log.i("error", it.message!!)
                    }
                    ResponseStatus.LOADING -> {
                    }
                    ResponseStatus.SUCCESS -> {
                        DialogUtils.hideProgressDialog()
                        Log.i("preview", it.data!!)
                        handlingResponse(it.data!!)


                    }
                }

            }
        })





        binding.listeners = object : GenericListeners {
            override fun onTapClose() {
                dismiss()
            }

            override fun onTapGoLive() {
                if (validateFields()) {
                    DialogUtils.showProgressDialog(context!!)
                    val jsonObject = JSONObject().apply {
                        put("fbkey", binding.postStreamKey.text.toString())
                        put("action", "live")
                    }
                    viewModel.subscribeToTopic("${deviceId}/live", jsonObject)
                    /* if (headerName == "yt") {
                         googleSignedAccount?.let {
                             viewModel.callYoutubeLive(
                                 transport,
                                 jsonFactory,
                                 credential!!,
                                 binding.postTitleEditText.text.toString(),
                                 binding.postDescriptionEditText.text.toString()
                             )
                         }
                     } else {

                     }*/
                }
            }

            override fun onTapStopLive() {
                DialogUtils.showProgressDialog(context!!)
                val jsonObject = JSONObject().apply {
                    put("action", "stop")
                }
                viewModel.subscribeToTopic("${deviceId}/stop", jsonObject)

            }
        }
    }

    private fun handlingResponse(data: String) {
        val obj = JSONObject(data)
        if (obj.has("streaming") && obj.getString("streaming")
                .equals("start", ignoreCase = true)
        ) {
            showToast("Streaming has been started!")
            (activity as MainActivity).gotoActivity(
                PreviewVideoActivity::class.java,
                Constants.RTMP_URL,
                obj.getString("stream_link"),
                Constants.DEVICE_ID,
                deviceId
            )
        } else if (obj.has("streaming") && obj.getString("streaming")
                .equals("stop", ignoreCase = true)
        ) {
            showToast("Streaming has been stopped!")
            dismiss()
//            Utils.showAlertDialog(
//                context!!,
//                description = "Streaming has been stopped!",
//                title = "Stop!",
//                buttonText = "Ok"
//            )
        } else if (obj.has("error")) {
            showToast(obj.getString("error"))
            dismiss()
            /* Utils.showAlertDialog(
                 context!!,
                 description = obj.getString("error"),
                 title = "Error!",
                 buttonText = "Ok"
             )*/
        } else if (obj.has("live")) {
            showToast(obj.getString("live"))
            dismiss()
            /*   Utils.showAlertDialog(
                   context!!,
                   description = obj.getString("live"),
                   title = "Live!",
                   buttonText = "Ok"
               )*/
        }
    }

    private fun validateFields(): Boolean {
//        if (binding.postTitleEditText.text.isNullOrEmpty()) {
//            binding.postTitleEditTextLayout.error = "Required Field!"
//            return false
//        } else {
//            binding.postTitleEditTextLayout.isErrorEnabled = false
//        }
//
//        if (binding.postDescriptionEditText.text.isNullOrEmpty()) {
//            binding.postDescriptionEditTextLayout.error = "Required Field!"
//            return false
//        } else {
//            binding.postDescriptionEditTextLayout.isErrorEnabled = false
//        }
        if (binding.postStreamKey.text.isNullOrEmpty()) {
            binding.postStreamKeyLayout.error = "Required Field!"
            return false
        } else {
            binding.postStreamKeyLayout.isErrorEnabled = false
        }
        return true
    }

    companion object {
        @JvmStatic
        fun newInstance(headerName: String, deviceId: String) =
            PostCreationDialog().apply {
                arguments = Bundle().apply {
                    putString(GO_LIVE_INTENT_KEY, headerName)
                    putString(DEVICE_ID, deviceId)
                }
            }
    }

}