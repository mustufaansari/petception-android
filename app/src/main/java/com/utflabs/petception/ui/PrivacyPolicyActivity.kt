package com.utflabs.petception.ui

import android.os.Bundle
import android.view.MenuItem
import com.petception.base.utils.RemoteConstant
import com.utflabs.petception.R
import kotlinx.android.synthetic.main.activity_web_view_for_device_onbaording.*

class PrivacyPolicyActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_privacy_policy)

        setSupportActionBar(toolbar)
        supportActionBar?.title = "Privacy Policy"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        webView.settings.javaScriptEnabled = true
        webView.loadUrl(RemoteConstant.URL_FOR_PRIVACY_POLICY)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}