package com.utflabs.petception.models

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


/**
 * Created by Mustufa Ansari on 07/01/2021.
 * Email : mustufaayub82@gmail.com
 */
class FacebookPageResponse {


    @SerializedName("data")
    @Expose
    val data: List<Data>? = null


    inner class Data {
        @SerializedName("access_token")
        @Expose
        val accessToken: String? = null

        @SerializedName("category")
        @Expose
        val category: String? = null

        @SerializedName("category_list")
        @Expose
        val categoryList: List<CategoryList>? = null

        @SerializedName("name")
        @Expose
        val name: String? = null

        @SerializedName("id")
        @Expose
        val id: String? = null

        @SerializedName("tasks")
        @Expose
        val tasks: List<String>? = null

    }

    inner class CategoryList {
        @SerializedName("id")
        @Expose
        val id: String? = null

        @SerializedName("name")
        @Expose
        val name: String? = null

    }

}