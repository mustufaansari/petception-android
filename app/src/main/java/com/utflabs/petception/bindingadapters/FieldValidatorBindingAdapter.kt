package com.utflabs.petception.bindingadapters

import androidx.databinding.BaseObservable
import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputLayout

object FieldValidatorBindingAdapter : BaseObservable() {


    @BindingAdapter("textFieldError")
    @JvmStatic
    fun textFieldError(view: TextInputLayout, errorMessage: String) {
        view.error = errorMessage
    }
}