package com.utflabs.petception.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.utflabs.petception.viewmodels.AllDevicesViewModel
import com.utflabs.petception.viewmodels.MainActivityViewModel
import com.utflabs.petception.viewmodels.AuthViewModel

@Suppress("UNCHECKED_CAST")
class ViewModelFactory : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>) =
        with(modelClass) {
            when {
                isAssignableFrom(AuthViewModel::class.java) -> AuthViewModel()
                isAssignableFrom(MainActivityViewModel::class.java) -> MainActivityViewModel()
                isAssignableFrom(AllDevicesViewModel::class.java) -> AllDevicesViewModel()
                else -> throw IllegalArgumentException("Unknown viewModel class $modelClass")
            }
        } as T


    companion object {
        @Volatile
        private var instance: ViewModelFactory? = null

        fun getInstance() =
            instance ?: synchronized(ViewModelFactory::class.java) {
                instance ?: ViewModelFactory().also { instance = it }
            }
    }
}