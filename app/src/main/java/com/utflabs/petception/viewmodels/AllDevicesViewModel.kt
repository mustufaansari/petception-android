package com.utflabs.petception.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.petception.base.model.response.GetUserDevicesResponse
import com.petception.base.networking.ResponseStatusCallbacks
import com.petception.base.utils.InjectUtils
import com.utflabs.petception.utils.Constants.ApiStatusCode.ERROR_PLEASE_TRY_AGAIN
import kotlinx.coroutines.Dispatchers
import java.lang.Exception

/**
 * Created by  Mustufa Ansari on 24/10/2020.
 * Email : mustufaayub82@gmail.com
 */
class AllDevicesViewModel : ViewModel() {
    private val repository = InjectUtils.getGeneralRepository


}