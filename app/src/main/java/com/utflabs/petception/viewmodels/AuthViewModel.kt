package com.utflabs.petception.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.amazonaws.mobileconnectors.cognitoidentityprovider.*
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ChallengeContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.MultiFactorAuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.SignUpHandler
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.UpdateAttributesHandler
import com.amazonaws.services.cognitoidentityprovider.model.UsernameExistsException
import com.petception.base.networking.ResponseStatusCallbacks
import com.petception.base.storage.AppPreferences
import com.petception.base.utils.InjectUtils
import com.utflabs.petception.utils.Constants.ApiStatusCode.ERROR_PLEASE_TRY_AGAIN
import org.apache.commons.lang3.StringUtils

class AuthViewModel : ViewModel() {
    private val repository = InjectUtils.getGeneralRepository

    private val _isUserRegister = MutableLiveData<ResponseStatusCallbacks<Boolean>>()
    val isUserRegister: LiveData<ResponseStatusCallbacks<Boolean>>
        get() = _isUserRegister

    private val _isUserLoggedIn = MutableLiveData<ResponseStatusCallbacks<Boolean>>()
    val isUserLoggedIn: LiveData<ResponseStatusCallbacks<Boolean>>
        get() = _isUserLoggedIn


    fun userSignup(userEmail: String, userPassword: String, userDOB: String, name: String) {
        _isUserRegister.value = ResponseStatusCallbacks.loading(data = null)
        val attr = CognitoUserAttributes()
        attr.addAttribute("name", name)
        attr.addAttribute("email", userEmail)
        attr.addAttribute("birthdate", userDOB)
        repository.userSignUp(userEmail, userPassword, attr, object : SignUpHandler {
            override fun onSuccess(
                user: CognitoUser?,
                signUpConfirmationState: Boolean,
                cognitoUserCodeDeliveryDetails: CognitoUserCodeDeliveryDetails?
            ) {
                if (!user?.userId.isNullOrEmpty()) {
                    _isUserRegister.value = ResponseStatusCallbacks.success(true)
                    AppPreferences.userID = user!!.userId
                } else {
                    AppPreferences.userID = StringUtils.EMPTY
                    _isUserRegister.value = ResponseStatusCallbacks.error(
                        false,
                        ERROR_PLEASE_TRY_AGAIN
                    )
                }
            }

            override fun onFailure(exception: Exception?) {
                AppPreferences.userID = StringUtils.EMPTY
                try {
                    _isUserRegister.value = ResponseStatusCallbacks.error(
                        null,
                        (exception as UsernameExistsException).errorMessage
                    )
                } catch (e: java.lang.Exception) {
                    _isUserRegister.value = ResponseStatusCallbacks.error(
                        null,
                        ERROR_PLEASE_TRY_AGAIN
                    )
                }

            }
        })
    }

    fun loginUser(userId: String, password: String) {
        _isUserLoggedIn.value = ResponseStatusCallbacks.loading(null)
        repository.userLogin(userId, password, object : AuthenticationHandler {
            override fun onSuccess(userSession: CognitoUserSession?, newDevice: CognitoDevice?) {
                if (userSession!!.isValid) {
                    AppPreferences.userID = userId
                    AppPreferences.accessToken = userSession.idToken.jwtToken!!
                    updateFCMID()
                } else {
                    _isUserLoggedIn.postValue(
                        ResponseStatusCallbacks.error(
                            false,
                            ERROR_PLEASE_TRY_AGAIN
                        )
                    )
                }

            }

            override fun onFailure(exception: Exception) {
                try {
                    _isUserLoggedIn.postValue(
                        ResponseStatusCallbacks.error(
                            false,
                            (exception as com.amazonaws.services.cognitoidentityprovider.model.NotAuthorizedException).errorMessage
                        )
                    )
                } catch (e: java.lang.Exception) {
                    _isUserLoggedIn.postValue(
                        ResponseStatusCallbacks.error(
                            false,
                            ERROR_PLEASE_TRY_AGAIN
                        )
                    )
                }
            }

            override fun getAuthenticationDetails(
                authenticationContinuation: AuthenticationContinuation?,
                userId: String?
            ) {
            }

            override fun authenticationChallenge(continuation: ChallengeContinuation?) {}
            override fun getMFACode(continuation: MultiFactorAuthenticationContinuation?) {}
        })
    }


    fun updateFCMID() {
        repository.updateFCMIDInCognito(AppPreferences.userID, object : UpdateAttributesHandler {
            override fun onSuccess(attributesVerificationList: MutableList<CognitoUserCodeDeliveryDetails>?) {
                _isUserLoggedIn.postValue(ResponseStatusCallbacks.success(true))
            }

            override fun onFailure(exception: java.lang.Exception?) {
                _isUserLoggedIn.postValue(
                    ResponseStatusCallbacks.error(
                        false,
                        ERROR_PLEASE_TRY_AGAIN
                    )
                )
            }
        })
    }


}