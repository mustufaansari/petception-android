package com.utflabs.petception.viewmodels

import android.util.Log
import androidx.lifecycle.*
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.http.HttpTransport
import com.google.api.client.json.JsonFactory
import com.google.api.services.youtube.YouTube
import com.petception.base.model.response.AddDeviceResponse
import com.petception.base.model.response.EventData
import com.petception.base.model.response.GetUserDevicesResponse
import com.petception.base.model.response.request.AddDeviceRequest
import com.petception.base.networking.ResponseStatusCallbacks
import com.petception.base.source.RoomRepository
import com.petception.base.storage.AppPreferences
import com.petception.base.storage.room.entites.Device
import com.petception.base.utils.InjectUtils
import com.utflabs.petception.service.PushStreamLiveService
import com.utflabs.petception.utils.Constants.ApiStatusCode.ERROR_PLEASE_TRY_AGAIN
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.eclipse.paho.client.mqttv3.*
import org.json.JSONObject
import java.util.*

class MainActivityViewModel : ViewModel() {
    private val repository = InjectUtils.getGeneralRepository
    private val roomRepository = InjectUtils.getRoomRepository

    private val _isDeviceAdded = MutableLiveData<ResponseStatusCallbacks<AddDeviceResponse>>()
    val isDeviceAdded: LiveData<ResponseStatusCallbacks<AddDeviceResponse>>
        get() = _isDeviceAdded

    private val _isConnectedToMQTT = MutableLiveData<ResponseStatusCallbacks<String>>()
    val isConnectedToMQTT: LiveData<ResponseStatusCallbacks<String>>
        get() = _isConnectedToMQTT

    private val _youTubeLiveStreaming = MutableLiveData<ResponseStatusCallbacks<EventData>>()
    val youTubeLiveStreaming: LiveData<ResponseStatusCallbacks<EventData>>
        get() = _youTubeLiveStreaming

    private val _startYoutubeStreaming = MutableLiveData<ResponseStatusCallbacks<String>>()
    val startYoutubeStreaming: LiveData<ResponseStatusCallbacks<String>>
        get() = _startYoutubeStreaming

    var getAllDevicesList = InjectUtils.getRoomRepository.getAllDevicesList().asLiveData()


    fun logout() {
        repository.logout()
    }

    init {
        connectMQTTClient()
    }

    fun startYoutubeStreaming(
        transport: HttpTransport,
        jsonFactory: JsonFactory,
        credentials: GoogleAccountCredential, broadcastId: String
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _startYoutubeStreaming.postValue(
                    ResponseStatusCallbacks.success(
                        repository.startStreaming(
                            transport, jsonFactory, credentials
                        )
                    )
                )
            } catch (e: Exception) {

                Log.e("MainActivityViewModel", e.toString())
                _startYoutubeStreaming.postValue(
                    ResponseStatusCallbacks.error(
                        data = null,
                        message = "0"
                    )
                )
            }
        }
    }

    private val _getAllUserDevices =
        MutableLiveData<ResponseStatusCallbacks<GetUserDevicesResponse>>()
    val getAllUserDevices: LiveData<ResponseStatusCallbacks<GetUserDevicesResponse>>
        get() = _getAllUserDevices

    fun getAllUserDevices() {
        _getAllUserDevices.value = ResponseStatusCallbacks.loading(data = null)
        viewModelScope.launch {
            try {
                _getAllUserDevices.value = ResponseStatusCallbacks.success(
                    repository.getAllUserDevices()
                )
            } catch (e: Exception) {
                _getAllUserDevices.value =
                    ResponseStatusCallbacks.error(data = null, message = ERROR_PLEASE_TRY_AGAIN)

            }
        }
    }

    fun insertDevices(list: List<GetUserDevicesResponse.Data>) {
        viewModelScope.launch(Dispatchers.IO) {
            list.forEach { item ->
                val device = Device(
                    deviceId = item.deviceId!!,
                    deviceName = item.deviceName!!,
                    userId = item.userId!!
                )
                roomRepository.insertDevice(device)
            }
        }
    }

    fun toggleDeviceStatus(
        deviceId: String,
        status: Boolean,
        listener: RoomRepository.OnDeviceStatusChangeListener
    ) {
        viewModelScope.launch(Dispatchers.Default) {
            roomRepository.setDeviceStatus(deviceId, status)
            withContext(Dispatchers.Main) {
                listener.onDeviceStatusChanged(deviceId, status)
            }
        }
    }


    fun addDevice(ssId: String, deviceId: String) {
        _isDeviceAdded.value = ResponseStatusCallbacks.loading(data = null)
        viewModelScope.launch {
            try {
                _isDeviceAdded.value = ResponseStatusCallbacks.success(
                    repository.addDevice(
                        AddDeviceRequest(
                            deviceId = deviceId,
                            deviceName = ssId,
                            userId = AppPreferences.userID
                        )
                    )
                )
            } catch (e: Exception) {
                _isDeviceAdded.value =
                    ResponseStatusCallbacks.error(data = null, message = ERROR_PLEASE_TRY_AGAIN)

            }
        }
    }

    fun connectMQTTClient() {
        _isConnectedToMQTT.value = ResponseStatusCallbacks.loading(data = null)
        repository.connectMQTTClient(object : MqttCallback {
            override fun messageArrived(topic: String?, message: MqttMessage?) {
                _isConnectedToMQTT.value = ResponseStatusCallbacks.success(message.toString())
            }

            override fun connectionLost(cause: Throwable?) {
            }

            override fun deliveryComplete(token: IMqttDeliveryToken?) {
            }
        })
    }

    fun subscribeToTopic(topic: String, objects: JSONObject) {
        repository.subscribeToTopic(topic, objects)
    }

    fun callYoutubeLive(
        transport: HttpTransport,
        jsonFactory: JsonFactory,
        credentials: GoogleAccountCredential
    ) {
        _youTubeLiveStreaming.value = ResponseStatusCallbacks.loading(data = null)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _youTubeLiveStreaming.postValue(
                    ResponseStatusCallbacks.success(
                        repository.startYouTubeStreaming(
                            transport,
                            jsonFactory,
                            credentials
                        )!!
                    )
                )
            } catch (e: Exception) {
                _youTubeLiveStreaming.postValue(
                    ResponseStatusCallbacks.error(
                        data = null,
                        message = ERROR_PLEASE_TRY_AGAIN
                    )
                )
            }
        }
    }

    fun stopYoutubeStreaming(
        transport: HttpTransport,
        jsonFactory: JsonFactory,
        credentials: GoogleAccountCredential
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                repository.stopYouTubeStreaming(
                    transport,
                    jsonFactory,
                    credentials
                )
            } catch (e: Exception) {

            }
        }
    }
}