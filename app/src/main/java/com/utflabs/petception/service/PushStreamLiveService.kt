package com.utflabs.petception.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.Scopes
import com.google.api.client.extensions.android.http.AndroidHttp
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.json.JsonFactory
import com.google.api.client.json.gson.GsonFactory
import com.google.api.client.util.ExponentialBackOff
import com.google.api.services.youtube.YouTubeScopes
import com.petception.base.networking.ResponseStatus
import com.petception.base.storage.AppPreferences
import com.utflabs.petception.R
import com.utflabs.petception.ui.SplashActivity
import com.utflabs.petception.utils.Constants
import com.utflabs.petception.utils.Constants.DIGITS.DIGIT_ZERO
import com.utflabs.petception.utils.Utils
import com.utflabs.petception.viewmodels.MainActivityViewModel

/**
 * Created by Mustufa Ansari on 08/02/2021.
 * Email : mustufaayub82@gmail.com
 */
class PushStreamLiveService : LifecycleService() {

    /**
     * notification manager to show/hide/update notification in status bar
     */
    private var notificationManager: NotificationManager? = null

    val transport = AndroidHttp.newCompatibleTransport()

    var credential: GoogleAccountCredential? = null

    val jsonFactory: JsonFactory = GsonFactory()

    private var viewModel: MainActivityViewModel? = null

    override fun onCreate() {
        super.onCreate()
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        viewModel = ViewModelProvider.NewInstanceFactory().create(MainActivityViewModel::class.java)

        val account = GoogleSignIn.getLastSignedInAccount(this)
        credential = GoogleAccountCredential.usingOAuth2(
            this, listOf(Scopes.PROFILE, YouTubeScopes.YOUTUBE)
        )
        credential?.backOff = ExponentialBackOff()
        credential?.selectedAccountName = account?.account?.name

        // Android O requires a Notification Channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.app_name)
            // Create the channel for the notification
            val mChannel =
                NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT)
            // Set the Notification Channel for the Notification Manager.
            notificationManager?.createNotificationChannel(mChannel)
        }
        startForeground(NOTIFICATION_ID, getNotification("Going Live On YouTube"))
        pushingStreamingLive()
        Log.e(TAG, "Starting pushing service")


    }

    private fun pushingStreamingLive() {
        viewModel?.startYoutubeStreaming(
            transport,
            jsonFactory,
            credential!!,
            AppPreferences.broadCasId!!
        )
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        viewModel?.startYoutubeStreaming?.observe(
            this,
            androidx.lifecycle.Observer {
                it?.let {
                    when (it.status) {
                        ResponseStatus.SUCCESS -> {
                            Log.e(TAG, "Successfully live on YouTube")
                            stopService("Successfully live on YouTube")
                        }
                        ResponseStatus.LOADING -> {
                        }
                        ResponseStatus.ERROR -> {
                            if (it.message!!.contentEquals("0")) {
                                Log.e(TAG, "receving null response, pushing live again")
                                Handler(Looper.getMainLooper()).postDelayed({
                                    pushingStreamingLive()
                                }, 20000)
                            } else {
                                stopService("Can't going live on YouTube")
                            }
                        }
                    }
                }
            })


        return super.onStartCommand(intent, flags, startId)
    }

    /**
     * Returns the [NotificationCompat] used as part of the foreground service.
     */
    private fun getNotification(message: String): Notification {
        val notificationIntent = Intent(this, SplashActivity::class.java)
        val pendingIntent =
            PendingIntent.getActivity(this, DIGIT_ZERO, notificationIntent, DIGIT_ZERO)

        val builder =
            NotificationCompat.Builder(this, Utils.getChannelIDForOnGoingNotification(this))
                .setContentTitle(Constants.Notifications.NOTIFICATION_CONTENT_TITLE)
                .setContentText(message)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                /** using importance high as [Notification.PRIORITY_HIGH] is [Deprecated] */
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setWhen(System.currentTimeMillis())
                .setStyle(NotificationCompat.BigTextStyle().bigText(message))

        // Set the Channel ID for Android O.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID) // Channel ID
        }

        return builder.build()
    }


    private fun stopService(message: String) {
        notificationManager?.notify(NOTIFICATION_ID, getNotification(message))
        getNotification(message)
        stopSelf()
        stopForeground(true)
    }

    companion object {
        private val TAG: String = PushStreamLiveService::class.java.simpleName

        /**
         * The name of the channel for notifications.
         */
        private val CHANNEL_ID = "channel_01"

        /**
         * The identifier for the notification displayed for the foreground service.
         */
        private const val NOTIFICATION_ID = 12345678

    }
}