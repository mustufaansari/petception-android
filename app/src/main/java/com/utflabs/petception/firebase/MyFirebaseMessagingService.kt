package com.utflabs.petception.firebase

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.petception.base.storage.AppPreferences
import com.utflabs.petception.R
import com.utflabs.petception.ui.SplashActivity
import com.utflabs.petception.utils.Constants.Notifications.DEVICE_ID
import com.utflabs.petception.utils.Constants.Notifications.DOUBLE_SLASH
import com.utflabs.petception.utils.Constants.Notifications.EVENT
import com.utflabs.petception.utils.Constants.Notifications.SLASH
import com.utflabs.petception.utils.Utils
import org.apache.commons.lang3.math.NumberUtils
import org.json.JSONObject

class MyFirebaseMessagingService : FirebaseMessagingService() {
    private val TAG = MyFirebaseMessagingService::class.java.simpleName
    private var mContext: MyFirebaseMessagingService? = null
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        mContext = this
        Log.d(TAG, "FirebaseMsgServiceData: " + remoteMessage.data["pinpoint.jsonBody"])
        if(!AppPreferences.isStreaming){
            if (remoteMessage.data.containsKey("pinpoint.jsonBody") && remoteMessage.data["pinpoint.jsonBody"] != null) {
                val obj = JSONObject(remoteMessage.data["pinpoint.jsonBody"]!!)
                val title = remoteMessage.data["pinpoint.notification.title"]
                val body = remoteMessage.data["pinpoint.notification.body"]
                createNotification(
                    mContext,
                    title, body,
                    obj.getString("deviceId"),
                    obj.getString("event")
                )
            }

        }

    }

    companion object {
        fun createNotification(
            context: Context?,
            title: String?,
            message: String?,
            deviceId: String,
            event: String
        ) {
            val nManager =
                context!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val soundUri =
                Uri.parse("${ContentResolver.SCHEME_ANDROID_RESOURCE}$DOUBLE_SLASH${context.packageName}$SLASH${R.raw.notification}")
            val builder: NotificationCompat.Builder =
                NotificationCompat.Builder(context, Utils.getChannelID(soundUri))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSound(soundUri)
                    .setAutoCancel(true)
            val targetIntent =
                Intent(context, SplashActivity::class.java).putExtra(DEVICE_ID, deviceId)
                    .putExtra(EVENT, event)
            val contentIntent = PendingIntent.getActivity(
                context,
                NumberUtils.INTEGER_ZERO,
                targetIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
            builder.setContentIntent(contentIntent)
            nManager.notify(NumberUtils.INTEGER_ZERO, builder.build())
        }
    }

    override fun onNewToken(token: String) {
        if (token.isNotEmpty()) {
            AppPreferences.fcmRegistrationId = token
        }
    }
}