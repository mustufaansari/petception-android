package com.petception.base.source.mqtt

import com.petception.base.utils.Constants.MQTTConstants.MQTT_PASSWORD
import com.petception.base.utils.Constants.MQTTConstants.MQTT_USERNAME
import com.petception.base.utils.InjectUtils
import org.eclipse.paho.client.mqttv3.*
import org.json.JSONObject

/**
 * Created by Mustufa Ansari on 20/10/2020.
 * Copyright (c) 2020 All rights reserved.
 */
class MQTTOperations : MQTTDataSource {

    override fun connectMQTTClient(callback: MqttCallback) {
        val options = MqttConnectOptions()
        options.userName = MQTT_USERNAME
        options.password = MQTT_PASSWORD
        InjectUtils.getMqttClient.setCallback(callback)
        InjectUtils.getMqttClient.connect(options, null, null)
    }

    override fun subscribeToTopic(topic: String, jsonObject: JSONObject) {
        InjectUtils.getMqttClient.subscribe(topic, 1, null, object : IMqttActionListener {
            override fun onSuccess(asyncActionToken: IMqttToken?) {
                publishToTopic(topic, jsonObject)
            }

            override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {

            }
        })
    }

    override fun publishToTopic(topic: String, jsonObject: JSONObject) {
        val message = MqttMessage()
        message.payload = jsonObject.toString().toByteArray()
        message.qos = 1
        message.isRetained = false
        InjectUtils.getMqttClient.publish(topic, message)
    }


}