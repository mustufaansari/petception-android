package com.petception.base.source

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserAttributes
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationDetails
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.SignUpHandler
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.UpdateAttributesHandler
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.http.HttpTransport
import com.google.api.client.json.JsonFactory
import com.google.api.services.youtube.YouTube
import com.petception.base.R
import com.petception.base.model.response.AddDeviceResponse
import com.petception.base.model.response.EventData
import com.petception.base.model.response.GetUserDevicesResponse
import com.petception.base.model.response.request.AddDeviceRequest
import com.petception.base.storage.AppPreferences
import com.petception.base.utils.InjectUtils
import com.petception.base.utils.YouTubeApi
import java.util.*

/**
 * created by Mustufa Ansari on 18,August,2020
 * Email : mustufaayub82@gmail.com
 */
class GeneralRemoteDataSource : GeneralDataSource {
    override fun userSignUp(
        userId: String,
        password: String,
        attributes: CognitoUserAttributes,
        handler: SignUpHandler
    ) {
        InjectUtils.getCognito.signUpInBackground(userId, password, attributes, null, handler)
    }

    override fun userLogin(userId: String, userPassword: String, handler: AuthenticationHandler) {
        Thread(Runnable {
            val authDetail = AuthenticationDetails(userId, userPassword, null)
            InjectUtils.getCognito.getUser(userId)
                .initiateUserAuthentication(authDetail, handler, true).run()
        }).start()

    }

    override fun logout() {
        InjectUtils.getCognito.getUser(AppPreferences.userID).signOut()
        AppPreferences.logoutUser()

    }

    override fun updateFCMIDInCognito(userId: String, callback: UpdateAttributesHandler) {
        val attribute = CognitoUserAttributes()
        attribute.addAttribute("custom:fcm_id", AppPreferences.fcmRegistrationId)
        InjectUtils.getCognito.getUser(userId).updateAttributes(attribute, callback)
    }

    override suspend fun getAllUserDevices(): GetUserDevicesResponse {
        return InjectUtils.getRetrofit.getUserDevice(
            AppPreferences.accessToken,
            AppPreferences.userID
        )
    }

    override suspend fun addDevice(addDeviceRequest: AddDeviceRequest): AddDeviceResponse {
        return InjectUtils.getRetrofit.addUserDevice(AppPreferences.accessToken, addDeviceRequest)
    }

    override suspend fun startYouTubeStreaming(
        transport: HttpTransport,
        jsonFactory: JsonFactory,
        credentials: GoogleAccountCredential
    ): EventData? {
        try {
            val youtube =
                YouTube.Builder(
                    transport, jsonFactory,
                    credentials
                ).setApplicationName(InjectUtils.resource.getString(R.string.app_name)).build()

            val date = Date().toString()
            YouTubeApi.createLiveEvent(
                youtube, "You are going to live on youtube at $date",
                "PetCeption Live - $date"
            )
            return YouTubeApi.getLiveEvents(youtube)
        } catch (e: Exception) {
            return null
        }
    }

    override suspend fun stopYouTubeStreaming(
        transport: HttpTransport,
        jsonFactory: JsonFactory,
        credentials: GoogleAccountCredential
    ) {
        try {
            val youtube =
                YouTube.Builder(
                    transport, jsonFactory,
                    credentials
                ).setApplicationName(InjectUtils.resource.getString(R.string.app_name)).build()

            val date = Date().toString()
//            YouTubeApi.createLiveEvent(
//                youtube, "You are going to live on youtube at $date",
//                "PetCeption Live - $date"
//            )
            YouTubeApi.endEvent(youtube)
        } catch (e: Exception) {

        }
    }

    override suspend fun startStreaming(
        transport: HttpTransport,
        jsonFactory: JsonFactory,
        credentials: GoogleAccountCredential
    ): String {
        val youtube =
            YouTube.Builder(
                transport, jsonFactory,
                credentials
            ).setApplicationName(InjectUtils.resource.getString(R.string.app_name)).build()
        return YouTubeApi.startEvent(youtube)
    }
}