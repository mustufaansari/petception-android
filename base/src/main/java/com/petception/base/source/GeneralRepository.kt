package com.petception.base.source

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserAttributes
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.SignUpHandler
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.UpdateAttributesHandler
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.http.HttpTransport
import com.google.api.client.json.JsonFactory
import com.petception.base.model.response.AddDeviceResponse
import com.petception.base.model.response.EventData
import com.petception.base.model.response.GetUserDevicesResponse
import com.petception.base.model.response.request.AddDeviceRequest
import com.petception.base.source.mqtt.MQTTDataSource
import com.petception.base.source.mqtt.MQTTOperations
import org.eclipse.paho.client.mqttv3.IMqttActionListener
import org.eclipse.paho.client.mqttv3.MqttCallback
import org.json.JSONObject

/**
 * created by Mustufa Ansari on 18,August,2020
 * Email : mustufaayub82@gmail.com
 */
class GeneralRepository(
    private var generalRemoteDataSource: GeneralRemoteDataSource,
    private var mqttOperations: MQTTOperations
) : GeneralDataSource, MQTTDataSource {

    companion object {
        private var INSTANCE: GeneralRepository? = null

        /**
         * Returns the single instance of this class, creating it if necessary.
         * @param generalRemoteDataSource the backend data source
         * *
         * @param generalLocalDataSource  the device storage data source
         * *
         * @return the [JobsRepository] instance
         */
        @JvmStatic
        fun get(
            generalRemoteDataSource: GeneralRemoteDataSource,
            getMQTTOperations: MQTTOperations
        ) = INSTANCE ?: synchronized(GeneralRepository::class.java) {
            INSTANCE ?: GeneralRepository(generalRemoteDataSource, getMQTTOperations).also {
                INSTANCE = it
            }
        }

        /**
         * Used to force [get] to create a new instance
         * next time it's called.
         */
        @JvmStatic
        fun destroyInstance() {
            INSTANCE = null
        }
    }

    override fun userSignUp(
        userId: String,
        password: String,
        attributes: CognitoUserAttributes,
        handler: SignUpHandler
    ) {
        return generalRemoteDataSource.userSignUp(userId, password, attributes, handler)
    }

    override fun userLogin(userId: String, userPassword: String, handler: AuthenticationHandler) {
        return generalRemoteDataSource.userLogin(userId, userPassword, handler)
    }

    override fun logout() {
        return generalRemoteDataSource.logout()
    }

    override fun updateFCMIDInCognito(userId: String, callback: UpdateAttributesHandler) {
        generalRemoteDataSource.updateFCMIDInCognito(userId, callback)
    }

    override suspend fun getAllUserDevices(): GetUserDevicesResponse {
        return generalRemoteDataSource.getAllUserDevices()
    }

    override suspend fun addDevice(addDeviceRequest: AddDeviceRequest): AddDeviceResponse {
        return generalRemoteDataSource.addDevice(addDeviceRequest)
    }

    override suspend fun startYouTubeStreaming(
        transport: HttpTransport,
        jsonFactory: JsonFactory,
        credentials: GoogleAccountCredential
    ): EventData? {
        return generalRemoteDataSource.startYouTubeStreaming(
            transport,
            jsonFactory,
            credentials
        )
    }

    override suspend fun stopYouTubeStreaming(
        transport: HttpTransport,
        jsonFactory: JsonFactory,
        credentials: GoogleAccountCredential
    ) {
        generalRemoteDataSource.stopYouTubeStreaming(
            transport,
            jsonFactory,
            credentials
        )
    }

    override suspend fun startStreaming(
        transport: HttpTransport,
        jsonFactory: JsonFactory,
        credentials: GoogleAccountCredential
    ): String {
        return generalRemoteDataSource.startStreaming(
            transport,
            jsonFactory,
            credentials
        )
    }

    override fun connectMQTTClient(callback: MqttCallback) {
        mqttOperations.connectMQTTClient(callback)
    }

    override fun subscribeToTopic(topic: String, jsonObject: JSONObject) {
        mqttOperations.subscribeToTopic(topic, jsonObject)
    }

    override fun publishToTopic(topic: String, jsonObject: JSONObject) {
        TODO("Not yet implemented")
    }


}