package com.petception.base.source

import com.petception.base.source.mqtt.RoomDataSource
import com.petception.base.storage.room.dao.GeneralDao
import com.petception.base.storage.room.entites.Device
import kotlinx.coroutines.flow.Flow

/**
 * Created by Mustufa Ansari on 14/12/2021.
 * Email : mustufaayub82@gmail.com
 */
class RoomRepository(private val generalDao: GeneralDao) : RoomDataSource {
    override suspend fun insertDevice(device: Device) {
        generalDao.insertDevice(device)
    }

    override fun getAllDevicesList(): Flow<List<Device>> {
        return generalDao.getAllDevicesList()
    }

    override suspend fun setDeviceStatus(deviceId: String, status: Boolean) {
        generalDao.deviceStartedPreview(deviceId, status)
    }

    interface OnDeviceStatusChangeListener {
        fun onDeviceStatusChanged(deviceId: String, status: Boolean)
    }
}