package com.petception.base.source

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserAttributes
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.SignUpHandler
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.UpdateAttributesHandler
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.http.HttpTransport
import com.google.api.client.json.JsonFactory
import com.petception.base.model.response.AddDeviceResponse
import com.petception.base.model.response.EventData
import com.petception.base.model.response.GetUserDevicesResponse
import com.petception.base.model.response.request.AddDeviceRequest

/**
 * created by Mustufa Ansari on 18,August,2020
 * Email : mustufaayub82@gmail.com
 */
interface GeneralDataSource {

    fun userSignUp(
        userId: String,
        password: String,
        attributes: CognitoUserAttributes,
        handler: SignUpHandler
    )

    fun userLogin(userId: String, userPassword: String, handler: AuthenticationHandler)

    fun logout()

    fun updateFCMIDInCognito(userId: String, callback: UpdateAttributesHandler)

    suspend fun getAllUserDevices(): GetUserDevicesResponse

    suspend fun addDevice(addDeviceRequest: AddDeviceRequest): AddDeviceResponse

    suspend fun startYouTubeStreaming(
        transport: HttpTransport,
        jsonFactory: JsonFactory,
        credentials: GoogleAccountCredential
    ): EventData?

    suspend fun stopYouTubeStreaming(
        transport: HttpTransport,
        jsonFactory: JsonFactory,
        credentials: GoogleAccountCredential
    )

    suspend fun startStreaming(
        transport: HttpTransport,
        jsonFactory: JsonFactory,
        credentials: GoogleAccountCredential
    ): String
}