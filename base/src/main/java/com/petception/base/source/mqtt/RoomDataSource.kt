package com.petception.base.source.mqtt

import com.petception.base.storage.room.entites.Device
import kotlinx.coroutines.flow.Flow

/**
 * Created by Mustufa Ansari on 14/12/2021.
 * Email : mustufaayub82@gmail.com
 */
interface RoomDataSource {

    suspend fun insertDevice(device: Device)

     fun getAllDevicesList() : Flow<List<Device>>

     suspend fun setDeviceStatus(deviceId : String,status : Boolean)
}