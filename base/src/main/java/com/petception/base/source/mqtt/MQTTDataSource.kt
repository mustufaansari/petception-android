package com.petception.base.source.mqtt

import org.eclipse.paho.client.mqttv3.MqttCallback
import org.json.JSONObject

/**
 * Created by Mustufa Ansari on 20/10/2020.
 * Copyright (c) 2020 All rights reserved.
 */
interface MQTTDataSource {

    fun connectMQTTClient(callback: MqttCallback)

    fun subscribeToTopic(topic: String, jsonObject: JSONObject)

    fun publishToTopic(topic: String, jsonObject: JSONObject)
}