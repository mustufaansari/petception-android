package com.petception.base.model.response

import com.google.gson.annotations.SerializedName

class AddDeviceResponse {
    @field:SerializedName("success")
    val success: Boolean? = null

    @field:SerializedName("message")
    val message: String? = null
}

