package com.petception.base.model.response

/**
 * Created by Mustufa Ansari on 14/10/2020.
 * Copyright (c) 2020 All rights reserved.
 */
data class Wifi(
    val ssId: String,
    val bssId: String,
    val isConnectedToWIFI: Boolean?,
    val isConnectedToMobile: Boolean?
)