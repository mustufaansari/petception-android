package com.petception.base.model.response.request

import com.google.gson.annotations.SerializedName

data class DeleteDeviceRequest(

    @field:SerializedName("deviceId")
    val deviceId: String? = null,

    @field:SerializedName("userId")
    val userId: String? = null
)
