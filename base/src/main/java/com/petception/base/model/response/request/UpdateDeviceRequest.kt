package com.petception.base.model.response.request

import com.google.gson.annotations.SerializedName

data class UpdateDeviceRequest(

    @field:SerializedName("deviceId")
    val deviceId: String? = null,

    @field:SerializedName("deviceName")
    val deviceName: String? = null,

    @field:SerializedName("userId")
    val userId: String? = null
)
