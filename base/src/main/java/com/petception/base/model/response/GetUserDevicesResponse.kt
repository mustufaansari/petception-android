package com.petception.base.model.response

import com.google.gson.annotations.SerializedName

class GetUserDevicesResponse {

    @field:SerializedName("data")
    val data: List<Data>? = null

    @field:SerializedName("success")
    val success: Boolean? = null

    inner class Data {
        @field:SerializedName("deviceId")
        val deviceId: String? = null

        @field:SerializedName("deviceName")
        val deviceName: String? = null

        @field:SerializedName("userId")
        val userId: String? = null
        var isActive: Boolean = false

        var isDeviceActive : Boolean = false
    }
}

