package com.petception.base.utils

import android.app.Application
import android.content.res.AssetManager
import android.content.res.Resources
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool
import com.petception.base.DaggerApp
import com.petception.base.networking.ApiHostIUrls
import com.petception.base.source.GeneralRepository
import com.petception.base.source.RoomRepository
import org.eclipse.paho.android.service.MqttAndroidClient

/**
 * created by Mustufa Ansari on 18,August,2020
 * Email : mustufaayub82@gmail.com
 */
object InjectUtils {
    val appContext: Application
        get() = DaggerApp.component?.getBaseContext()!!

    val resource: Resources
        get() = DaggerApp.component?.getResources()!!

    val asset: AssetManager
        get() = DaggerApp.component?.getAssets()!!

    val getRetrofit: ApiHostIUrls
        get() = DaggerApp.component?.getRetrofit()!!

    val getGeneralRepository: GeneralRepository
        get() = DaggerApp.component?.getGeneralRepository()!!

    val getCognito: CognitoUserPool
        get() = DaggerApp.component?.getCognito()!!

    val getMqttClient: MqttAndroidClient
        get() = DaggerApp.component?.getMQTTClient()!!

    val getRoomRepository: RoomRepository
        get() = DaggerApp.component?.getRoomRepository()!!
}