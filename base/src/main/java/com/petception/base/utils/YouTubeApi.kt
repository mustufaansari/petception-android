package com.petception.base.utils

import android.util.Log
import com.google.api.client.googleapis.json.GoogleJsonResponseException
import com.google.api.client.util.DateTime
import com.google.api.services.youtube.YouTube
import com.google.api.services.youtube.model.*
import com.petception.base.model.response.EventData
import com.petception.base.storage.AppPreferences
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


object YouTubeApi {
    private const val FUTURE_DATE_OFFSET_MILLIS = 5 * 1000
    fun createLiveEvent(
        youtube: YouTube, description: String?,
        name: String?
    ) {
        val dateFormat = SimpleDateFormat(
            "yyyy-MM-dd'T'HH:mm:ss'Z'"
        )
        dateFormat.timeZone = TimeZone.getTimeZone("UTC")
        val futureDateMillis = (System.currentTimeMillis()
                + FUTURE_DATE_OFFSET_MILLIS)
        val futureDate = Date()
        futureDate.time = futureDateMillis
        val date = dateFormat.format(futureDate)
        Log.i(
            "YouTubeApi", String.format(
                "Creating event: name='%s', description='%s', date='%s'.",
                name, description, date
            )
        )
        try {
            val broadcastSnippet =
                LiveBroadcastSnippet()

            broadcastSnippet.title = name
            broadcastSnippet.description = description
            broadcastSnippet.scheduledStartTime = DateTime(futureDate)

            val contentDetails =
                LiveBroadcastContentDetails()

            val monitorStream =
                MonitorStreamInfo()

            monitorStream.enableMonitorStream = true
            contentDetails.monitorStream = monitorStream
            contentDetails.set("enableAutoStart",true)
            contentDetails.set("enableAutoStop",true)
            contentDetails.set("isReusable",true)



            // Create LiveBroadcastStatus with privacy status.
            val status =
                LiveBroadcastStatus()
            status.privacyStatus = "public"
            status.set("selfDeclaredMadeForKids",true)



            val broadcast =
                LiveBroadcast()

            broadcast.kind = "youtube#liveBroadcast"
            broadcast.snippet = broadcastSnippet
            broadcast.status = status
            broadcast.contentDetails = contentDetails





            // Create the insert request
            val liveBroadcastInsert =
                youtube
                    .liveBroadcasts().insert(
                        "snippet,status,contentDetails",
                        broadcast
                    )



            // Request is executed and inserted broadcast is returned
            val returnedBroadcast =
                liveBroadcastInsert.execute()
            AppPreferences.broadCasId = returnedBroadcast.id


            // Create a snippet with title.
            val streamSnippet =
                LiveStreamSnippet()
            streamSnippet.title = name


            // Create content distribution network with format and ingestion
            // type.
            val cdn =
                CdnSettings()
            cdn.ingestionType = "rtmp"
            cdn.frameRate = "30fps"
            cdn.resolution = "240p"
            val stream =
                LiveStream()
            stream.kind = "youtube#liveStream"
            stream.snippet = streamSnippet
            stream.cdn = cdn


            // Create the insert request
            val liveStreamInsert =
                youtube.liveStreams()
                    .insert("snippet,cdn", stream)




            // Request is executed and inserted stream is returned
            val returnedStream =
                liveStreamInsert.execute()

            Log.i("returnedBroadcast", returnedBroadcast.id)

            // Create the bind request
            val liveBroadcastBind =
                youtube
                    .liveBroadcasts().bind(
                        returnedBroadcast.id,
                        "id,contentDetails"
                    )

            // Set stream id to bind
            liveBroadcastBind.streamId = returnedStream.id

            AppPreferences.streamId = returnedStream.id
            Log.i("returnedStream", returnedStream.id)

            // Request is executed and bound broadcast is returned
            liveBroadcastBind.execute()

        } catch (e: GoogleJsonResponseException) {
            System.err.println(
                "GoogleJsonResponseException code: "
                        + e.details.code + " : "
                        + e.details.message
            )
            e.printStackTrace()
        } catch (e: IOException) {
            System.err.println("IOException: " + e.message)
            e.printStackTrace()
        } catch (t: Throwable) {
            System.err.println("IOException: " + t.stackTrace)
            t.printStackTrace()
        }
    }

    fun getLiveEvents(
        youtube: YouTube
    ): EventData? {
        Log.i("YouTubeApi", "Requesting live events.")
        val liveBroadcastRequest =
            youtube
                .liveBroadcasts().list("id,snippet,contentDetails")
        liveBroadcastRequest.broadcastStatus = "upcoming"

        // List request is executed and list of broadcasts are returned
        val returnedListResponse =
            liveBroadcastRequest.execute()

        Log.i("YouTubeApi", returnedListResponse.items.toString())


        val returnedList = returnedListResponse.items
        val event = EventData()
        var liveBroadCast = LiveBroadcast()
        returnedList.forEach {
            if (AppPreferences.streamId == it.contentDetails.boundStreamId) {
                liveBroadCast = it
            }
        }
        event.event = liveBroadCast
        val ingestionAddress: String = getIngestionAddress(youtube, AppPreferences.streamId)
        event.ingestionAddress = ingestionAddress
        return event

//        // List request is executed and list of broadcasts are returned
//        val returnedListResponse =
//            liveBroadcastRequest.execute()
//
//        Log.i("YouTubeApi", returnedListResponse.items.toString())
//
//        // Get the list of broadcasts associated with the user.
//
//        val returnedList = returnedListResponse.items
//
//        val resultList: MutableList<EventData> =
//            ArrayList(returnedList.size)
//        var event: EventData
//
//        for (broadcast in returnedList) {
//            event = EventData()
//            event.event = broadcast
//            val streamId = broadcast.contentDetails.boundStreamId
//
//            if (streamId != null) {
//                val ingestionAddress =
//                    getIngestionAddress(youtube, streamId)
//                event.ingestionAddress = ingestionAddress
//            }
//            resultList.add(event)
//        }
//        return resultList

    }

    @Throws(IOException::class)
    fun getIngestionAddress(
        youtube: YouTube,
        streamId: String?
    ): String {
        val liveStreamRequest =
            youtube.liveStreams()
                .list("cdn")
        liveStreamRequest.id = streamId
        val returnedStream =
            liveStreamRequest.execute()
        val streamList =
            returnedStream.items
        if (streamList.isEmpty()) {
            return ""
        }
        val ingestionInfo =
            streamList[0].cdn.ingestionInfo
        return (ingestionInfo.ingestionAddress + "/"
                + ingestionInfo.streamName)
    }


    fun endEvent(youtube: YouTube) {
        val transitionRequest = youtube.liveBroadcasts().transition(
            "complete", AppPreferences.broadCasId, "status"
        )
        transitionRequest.execute()
    }

    fun startEvent(youtube: YouTube): String {
        val transitionRequest = youtube.liveBroadcasts().transition(
            "live", AppPreferences.broadCasId, "status"
        )
        return transitionRequest.execute().toString()
    }

}