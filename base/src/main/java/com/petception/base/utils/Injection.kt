package com.petception.base.utils

import com.petception.base.source.GeneralRemoteDataSource
import com.petception.base.source.GeneralRepository
import com.petception.base.source.mqtt.MQTTOperations

/**
 * created by Mustufa Ansari on 18,August,2020
 * Email : mustufaayub82@gmail.com
 */
object Injection {

    fun provideGeneralRepository(): GeneralRepository {
        return GeneralRepository(GeneralRemoteDataSource(), MQTTOperations())
    }
}
