package com.petception.base.utils

import android.os.Handler
import android.os.Looper
import android.view.Gravity
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.androidadvance.topsnackbar.TSnackbar
import com.petception.base.BuildConfig
import com.petception.base.R


object AppToast {
    private var mToast: Toast? = null
    private var snackbar: TSnackbar? = null

    fun showToast(toastMessage: String) {
        createToast(toastMessage, Toast.LENGTH_SHORT)
    }

    fun showToast(@StringRes resId: Int) {
        createToast(InjectUtils.appContext.getString(resId), Toast.LENGTH_SHORT)
    }

    fun showDebugToast(toastMessage: String) {
        if (BuildConfig.DEBUG)
            createToast(toastMessage, Toast.LENGTH_SHORT)
    }

    fun showLongToast(toastMessage: String) {
        createToast(toastMessage, Toast.LENGTH_LONG)
    }

    fun showLongToast(@StringRes resId: Int) {
        createToast(InjectUtils.appContext.getString(resId), Toast.LENGTH_LONG)
    }

    private fun createToast(string: String?, toastDuration: Int) {
        mToast?.cancel()
        mToast = Toast.makeText(InjectUtils.appContext, string, toastDuration)
        Handler(Looper.getMainLooper()).post {
            try {
                mToast?.show()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun showSnackBar(view: View, stringToShow: String) {
        createSnackBar(view, stringToShow, TSnackbar.LENGTH_SHORT)
    }

    private fun createSnackBar(view: View, messageToShow: String, duration: Int) {
        snackbar?.dismiss()
        snackbar = TSnackbar.make(view, messageToShow, duration)
        snackbar!!.setMaxWidth(3000)
        snackbar!!.duration = TSnackbar.LENGTH_SHORT
        val snackbarView = snackbar!!.view
        snackbarView.setBackgroundColor(
            ContextCompat.getColor(
                InjectUtils.appContext,
                R.color.lightblue
            )
        )
        val textView =
            snackbarView.findViewById<View>(com.androidadvance.topsnackbar.R.id.snackbar_text) as TextView
        textView.setLines(2)
        textView.setTextColor(ContextCompat.getColor(InjectUtils.appContext, android.R.color.white))
        textView.gravity = Gravity.CENTER_HORIZONTAL
        Handler(Looper.getMainLooper()).post {
            snackbar?.show()
        }
    }
}