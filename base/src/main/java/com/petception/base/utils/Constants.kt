package com.petception.base.utils

import com.amazonaws.regions.Regions

/**
 * created by Mustufa Ansari on 18,August,2020
 * Email : mustufaayub82@gmail.com
 */
object Constants {

    object SharedPreferenceConfig {
        const val SHARED_PREFERENCES_NAME = "perception-android"

    }

    object RetrofitConstants {
        const val RETROFIT_METHOD_POST = "post"
        const val RETROFIT_METHOD_GET = "get"
    }

    object RoomDatabaseConfig {
        const val DATABASE_NAME = "perception-android.db"
        const val DATABASE_VERSION = 2

    }


    object AppPreferenceKeys {
        const val IS_USER_LOGGED_IN = "IS_USER_LOGGED_IN"
        const val USER_ID = "USER_ID"
        const val ACCESS_TOKEN = "ACCESS_TOKEN"
        const val FCM_REGISTRATION_ID = "FCM_REGISTRATION_ID"
        const val YOUTUBE_BROAD_CAST_ID = "YOUTUBE_BROAD_CAST_ID"
        const val YOUTUBE_STREAM_ID = "YOUTUBE_STREAM_ID"
        const val WIFI_INFO = "WIFI_INFO"
        const val IS_STREAMING = "IS_STREAMING"

    }

    object CognitoConstants {
        const val POOL_ID = "us-east-2_h3M4znr7P"
        const val CLIENT_SECRET = "u2geakmlru2mmqjj0e6a17eorelc3n5nlglgqka4sqin7qth0ji"
        const val CLIENT_ID = "6fjn5pbummj8ipgedcb78jvnvp"
        val AWS_REGION: Regions = Regions.US_EAST_2
    }

    object MQTTConstants {
        const val MQTT_BROKER_URL = "tcp://3.132.130.234:4040"
        const val MQTT_USERNAME = "petCeption"
        val MQTT_PASSWORD = "D0dg3rs420#".toCharArray()
    }

}