package com.petception.base.utils

/**
 * created by Mustufa Ansari on 18,August,2020
 * Email : mustufaayub82@gmail.com
 */
object RemoteConstant {
    const val REMOTE_URL = "https://rfzgxxkfi9.execute-api.us-east-2.amazonaws.com/Testing/"
    const val URL_FOR_ON_BOARDING = "http://10.0.0.1"
    const val URL_FOR_PRIVACY_POLICY = "https://sites.google.com/view/petception/home"

}