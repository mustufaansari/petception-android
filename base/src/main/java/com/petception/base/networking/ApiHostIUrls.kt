package com.petception.base.networking

import com.petception.base.model.response.AddDeviceResponse
import com.petception.base.model.response.GetUserDevicesResponse
import com.petception.base.model.response.request.AddDeviceRequest
import com.petception.base.model.response.request.DeleteDeviceRequest
import com.petception.base.networking.ApiFields.AUTHORIZATION
import retrofit2.http.*

/**
 * created by Mustufa Ansari on 18,August,2020
 * Email : mustufaayub82@gmail.com
 */
interface ApiHostIUrls {

    @POST(ApiRoutes.ADD_DEVICES)
    suspend fun addUserDevice(
        @Header(AUTHORIZATION) header: String?,
        @Body addDeviceRequest: AddDeviceRequest
    ): AddDeviceResponse

    @GET(ApiRoutes.GET_DEVICES)
    suspend fun getUserDevice(
        @Header(AUTHORIZATION) header: String?, @Path(value = "userId") userID: String?
    ): GetUserDevicesResponse

    @DELETE
    suspend fun deleteUserDevice(@Body deleteDeviceRequest: DeleteDeviceRequest)
}