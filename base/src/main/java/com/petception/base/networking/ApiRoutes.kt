package com.petception.base.networking

/**
 * created by Mustufa Ansari on 18,August,2020
 * Email : mustufaayub82@gmail.com
 */
object ApiRoutes {

    const val GET_DEVICES = "devices/{userId}"
    const val ADD_DEVICES = "devices/"
}