package com.petception.base.daggar.module

import android.app.Application
import com.amazonaws.ClientConfiguration
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool
import com.petception.base.utils.Constants.CognitoConstants.AWS_REGION
import com.petception.base.utils.Constants.CognitoConstants.CLIENT_ID
import com.petception.base.utils.Constants.CognitoConstants.CLIENT_SECRET
import com.petception.base.utils.Constants.CognitoConstants.POOL_ID
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class CognitoModule {

    @Provides
    @Singleton
    fun getCognitoInstance(context:Application,clientConfiguration: ClientConfiguration) : CognitoUserPool{
        return CognitoUserPool(context.applicationContext,POOL_ID,CLIENT_ID,CLIENT_SECRET,clientConfiguration,AWS_REGION)
    }


    @Provides
    @Singleton
    fun getClientConfiguration() : ClientConfiguration{
        return ClientConfiguration()
    }
}