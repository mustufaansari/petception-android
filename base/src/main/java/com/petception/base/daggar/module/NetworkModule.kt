package com.petception.base.daggar.module

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.petception.base.networking.ApiHostIUrls
import com.petception.base.utils.NetworkUtil
import com.petception.base.utils.RemoteConstant
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * created by Mustufa Ansari on 20,August,2020
 * Email : mustufaayub82@gmail.com
 */
@Module
class NetworkModule {
    @Provides
    @Singleton
    fun getRetrofitInstance(
        client: OkHttpClient,
        gsonConverterFactory: GsonConverterFactory,
        coroutineCallAdapterFactory: CoroutineCallAdapterFactory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(RemoteConstant.REMOTE_URL)
            .addCallAdapterFactory(coroutineCallAdapterFactory)
            .addConverterFactory(gsonConverterFactory)
            .client(client)
            .build()
    }

    @Provides
    @Singleton
    fun provideAPIClient(retrofit: Retrofit): ApiHostIUrls {
        return retrofit.create(ApiHostIUrls::class.java)
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        return NetworkUtil.enableTls12OnPreLollipop().build()
    }

    @Provides
    @Singleton
    fun getGsonConverterFactory(): GsonConverterFactory {
        return GsonConverterFactory.create()
    }

    @Provides
    @Singleton
    fun getCoroutineCallAdapter() : CoroutineCallAdapterFactory{
        return CoroutineCallAdapterFactory.invoke()
    }
}