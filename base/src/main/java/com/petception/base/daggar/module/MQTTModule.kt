package com.petception.base.daggar.module

import android.app.Application
import com.petception.base.utils.Constants.MQTTConstants.MQTT_BROKER_URL
import dagger.Module
import dagger.Provides
import org.eclipse.paho.android.service.MqttAndroidClient
import org.eclipse.paho.client.mqttv3.MqttClient
import javax.inject.Singleton

/**
 * Created by Mustufa Ansari on 20/10/2020.
 * Copyright (c) 2020 All rights reserved.
 */

@Module
class MQTTModule {

    @Provides
    @Singleton
    fun getMQTTClient(context: Application): MqttAndroidClient {
        return MqttAndroidClient(context, MQTT_BROKER_URL, MqttClient.generateClientId())
    }
}