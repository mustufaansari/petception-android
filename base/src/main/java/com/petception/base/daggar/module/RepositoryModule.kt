package com.petception.base.daggar.module

import com.petception.base.source.GeneralRemoteDataSource
import com.petception.base.source.GeneralRepository
import com.petception.base.source.mqtt.MQTTOperations
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * created by Mustufa Ansari on 20,August,2020
 * Email : mustufaayub82@gmail.com
 */
@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun getGeneralRepository(generalRemoteDataSource: GeneralRemoteDataSource,mqttOperations: MQTTOperations): GeneralRepository {
        return GeneralRepository(generalRemoteDataSource,mqttOperations)
    }

    @Provides
    @Singleton
    fun getGeneralRemoteDataSource(): GeneralRemoteDataSource {
        return GeneralRemoteDataSource()
    }

    @Provides
    @Singleton
    fun getMQTTOperations(): MQTTOperations {
        return MQTTOperations()
    }
}