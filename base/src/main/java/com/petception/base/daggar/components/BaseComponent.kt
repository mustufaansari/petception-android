package com.petception.base.daggar.components

import android.app.Application
import android.content.SharedPreferences
import android.content.res.AssetManager
import android.content.res.Resources
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool
import com.petception.base.daggar.module.*
import com.petception.base.networking.ApiHostIUrls
import com.petception.base.source.GeneralRepository
import com.petception.base.source.RoomRepository
import com.petception.base.storage.room.AppDatabase
import dagger.Component
import org.eclipse.paho.android.service.MqttAndroidClient
import javax.inject.Singleton

/**
 * created by Mustufa Ansari on 18,August,2020
 * Email : mustufaayub82@gmail.com
 */
@Singleton
@Component(
    modules = [LocalStorageModule::class,
        ContextHelperModule::class, NetworkModule::class,
        RepositoryModule::class, CognitoModule::class, MQTTModule::class],
    dependencies = [ContextComponent::class]
)
interface BaseComponent {
    /**
     * base application context
     */
    fun getBaseContext(): Application

    /**
     * android resources
     */
    fun getResources(): Resources

    /**
     * android assets
     */
    fun getAssets(): AssetManager

    /**
     * android shared preference instance
     */
    fun getSharedPref(): SharedPreferences

    /**
     * room database instance
     */
    fun getAppDatabase(): AppDatabase

    /**
     * retrofit Host Url instance for API calls
     */
    fun getRetrofit(): ApiHostIUrls

    /**
     * MVVM repository
     */
    fun getGeneralRepository(): GeneralRepository

    /**
     * cognito module for signup or signin
     */
    fun getCognito(): CognitoUserPool

    /**
     * MQTT client for realtime communication
     */

    fun getMQTTClient(): MqttAndroidClient

    /**
     * Room repo
     */
    fun getRoomRepository(): RoomRepository
}