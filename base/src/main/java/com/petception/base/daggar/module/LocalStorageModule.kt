package com.petception.base.daggar.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.petception.base.source.RoomRepository
import com.petception.base.storage.room.AppDatabase
import com.petception.base.utils.Constants.RoomDatabaseConfig.DATABASE_NAME
import com.petception.base.utils.Constants.SharedPreferenceConfig.SHARED_PREFERENCES_NAME
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * created by Mustufa Ansari on 18,August,2020
 * Email : mustufaayub82@gmail.com
 */
@Module
class LocalStorageModule {
    @Provides
    @Singleton
    fun getSharedPref(app: Application): SharedPreferences {
        return app.getSharedPreferences(
            app.applicationInfo.name + SHARED_PREFERENCES_NAME,
            Context.MODE_PRIVATE
        )
    }

    @Provides
    @Singleton
    fun getAppDatabase(app: Application): AppDatabase {
        return Room.databaseBuilder(
            app.applicationContext,
            AppDatabase::class.java, DATABASE_NAME
        ).fallbackToDestructiveMigration().build()
    }

    @Provides
    @Singleton
    fun getRoomRepository(appDatabase: AppDatabase): RoomRepository {
        return RoomRepository(appDatabase.generalDao())
    }


}