package com.petception.base.daggar.components

import android.app.Application
import com.petception.base.daggar.module.ContextModule
import dagger.Component

/**
 * created by Mustufa Ansari on 18,August,2020
 * Email : mustufaayub82@gmail.com
 */
@Component(modules = [ContextModule::class])
interface ContextComponent {
    fun getContext(): Application

}