package com.petception.base

import androidx.multidex.MultiDexApplication
import com.petception.base.daggar.components.BaseComponent
import com.petception.base.daggar.components.DaggerBaseComponent
import com.petception.base.daggar.components.DaggerContextComponent
import com.petception.base.daggar.module.ContextModule

/**
 * created by Mustufa Ansari on 18,August,2020
 * Email : mustufaayub82@gmail.com
 */
open class DaggerApp : MultiDexApplication() {

    companion object {
        var component: BaseComponent? = null
            private set
    }

    override fun onCreate() {
        super.onCreate()
        val applicationComponent = DaggerContextComponent
            .builder()
            .contextModule(ContextModule(this))
            .build()

        component = DaggerBaseComponent.builder()
            .contextComponent(applicationComponent)
            .build()


    }


}