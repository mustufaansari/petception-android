package com.petception.base.storage.room.entites

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import org.apache.commons.lang3.StringUtils

/**
 * Created by Mustufa Ansari on 14/12/2021.
 * Email : mustufaayub82@gmail.com
 */
@Entity(
    tableName = "device_table",
    indices = [Index(name = "index_deviceId", value = ["device_id"], unique = true)]
)
data class Device(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    @ColumnInfo(name = "device_name")
    var deviceName: String = StringUtils.EMPTY,
    @ColumnInfo(name = "device_id")
    var deviceId: String = StringUtils.EMPTY,
    @ColumnInfo(name = "userId")
    var userId: String = StringUtils.EMPTY,
    @ColumnInfo(name = "is_device_active")
    var isDeviceActive: Boolean = false,
    @ColumnInfo(name = "is_preview_active")
    var isPreviewActive: Boolean = false,
    @ColumnInfo(name = "battery_level")
    var batteryLevel: String = StringUtils.EMPTY

)