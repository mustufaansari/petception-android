package com.petception.base.storage.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.petception.base.storage.room.dao.GeneralDao
import com.petception.base.storage.room.entites.Device
import com.petception.base.utils.Constants.RoomDatabaseConfig.DATABASE_VERSION

/**
 * created by Mustufa Ansari on 18,August,2020
 * Email : mustufaayub82@gmail.com
 */
@Database(
    entities = [Device::class],
    version = DATABASE_VERSION,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun generalDao(): GeneralDao
}