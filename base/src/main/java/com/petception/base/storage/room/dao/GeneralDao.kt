package com.petception.base.storage.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.petception.base.storage.room.entites.Device
import kotlinx.coroutines.flow.Flow

/**
 * created by Mustufa Ansari on 18,August,2020
 * Email : mustufaayub82@gmail.com
 */
@Dao
interface GeneralDao {
    /**
     * inserting devices
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDevice(device: Device): Long


    @Query("SELECT * FROM device_table ")
    fun getAllDevicesList(): Flow<List<Device>>

    @Query("UPDATE device_table SET is_preview_active  = :status WHERE device_id = :deviceId")
    suspend fun deviceStartedPreview(deviceId: String, status: Boolean)
}
