package com.petception.base.storage

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.petception.base.model.response.Wifi
import com.petception.base.utils.Constants.AppPreferenceKeys.ACCESS_TOKEN
import com.petception.base.utils.Constants.AppPreferenceKeys.FCM_REGISTRATION_ID
import com.petception.base.utils.Constants.AppPreferenceKeys.IS_STREAMING
import com.petception.base.utils.Constants.AppPreferenceKeys.IS_USER_LOGGED_IN
import com.petception.base.utils.Constants.AppPreferenceKeys.USER_ID
import com.petception.base.utils.Constants.AppPreferenceKeys.WIFI_INFO
import com.petception.base.utils.Constants.AppPreferenceKeys.YOUTUBE_BROAD_CAST_ID
import com.petception.base.utils.Constants.AppPreferenceKeys.YOUTUBE_STREAM_ID
import org.apache.commons.lang3.StringUtils


object AppPreferences : AppPreferencesBase() {

    /**
     * Notification will turn off/on while streaming
     */
    var isStreaming: Boolean
        /**
         * [IS_STREAMING] return true if user is streaming or else false
         */
        get() = get(IS_STREAMING, false) as Boolean
        /**
         * set [IS_STREAMING] value
         */
        set(isStreaming) = put(IS_STREAMING, isStreaming)


    var userID: String
        /**
         * [USER_ID] return user id if user successfully signup or login
         */
        get() = get(USER_ID, StringUtils.EMPTY) as String
        /**
         * set [USER_ID] value
         */
        set(userID) = put(USER_ID, userID)


    var accessToken: String
        /**
         * [ACCESS_TOKEN] save access token on login
         */
        get() = get(ACCESS_TOKEN, StringUtils.EMPTY) as String
        /**
         * set [ACCESS_TOKEN] value
         */
        set(accessToken) = put(ACCESS_TOKEN, accessToken)


    var fcmRegistrationId: String?
        /**
         * Check If FCM Registration ID is available or not
         * @return If exit will return value else empty
         */
        get() {
            (get(FCM_REGISTRATION_ID, StringUtils.EMPTY) as String).also {
                return if (it.isNotEmpty()) it else null
            }
        }
        /**
         * Set FCM Registration ID
         * @param fcmRegistrationId: FCM Registration Id
         */
        set(fcmRegistrationId) = put(FCM_REGISTRATION_ID, fcmRegistrationId)

    var wifiInfo: Wifi?
        get() {
            val data: String = get(WIFI_INFO, StringUtils.EMPTY) as String
            if (data.isNotEmpty()) {
                return Gson().fromJson(data, object : TypeToken<Wifi>() {}.type)
            }
            return null
        }
        set(wifiInfo) = put(WIFI_INFO, Gson().toJson(wifiInfo))

    var broadCasId: String?
        get() {
            (get(YOUTUBE_BROAD_CAST_ID, StringUtils.EMPTY) as String).also {
                return if (it.isNotEmpty()) it else null
            }
        }
        set(broadCasId) = put(YOUTUBE_BROAD_CAST_ID, broadCasId)

    var streamId: String?
        get() {
            (get(YOUTUBE_STREAM_ID, StringUtils.EMPTY) as String).also {
                return if (it.isNotEmpty()) it else null
            }
        }
        set(streamId) = put(YOUTUBE_STREAM_ID, streamId)
}