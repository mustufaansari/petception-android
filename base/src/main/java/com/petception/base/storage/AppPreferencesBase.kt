package com.petception.base.storage

import com.petception.base.DaggerApp


/**
 * Shared preference instance and methods to put and get data from local source
 */
open class AppPreferencesBase {
    private var mSharedPreferences = DaggerApp.component!!.getSharedPref()

    fun put(key: String?, objectValue: Any?) {
        val editor = mSharedPreferences.edit()
        when (objectValue) {
            is String -> editor.putString(key, objectValue)
            is Int -> editor.putInt(key, objectValue)
            is Long -> editor.putLong(key, objectValue)
            is Boolean -> editor.putBoolean(key, objectValue)
            is Float -> editor.putFloat(key, objectValue)
            else -> editor.putString(key, objectValue.toString())
        }
        editor.apply()
    }

    operator fun contains(key: String): Boolean {
        return mSharedPreferences.contains(key)
    }

    operator fun get(key: String?, defaultObject: Any?): Any? {
        return when (defaultObject) {
            is String -> mSharedPreferences.getString(key, defaultObject)
            is Int -> mSharedPreferences.getInt(key, defaultObject)
            is Long -> mSharedPreferences.getLong(key, defaultObject)
            is Boolean -> mSharedPreferences.getBoolean(key, defaultObject)
            is Float -> mSharedPreferences.getFloat(key, defaultObject)
            else -> null
        }
    }


    fun logoutUser() {
        mSharedPreferences.edit().clear().apply()
    }


}